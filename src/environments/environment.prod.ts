export const environment = {
  production: true,
  apiURL: 'https://corecompliance.azurewebsites.net/api/',
  filesURL: 'https://corecompliance.azurewebsites.net/api/uploadfiles/'
};
