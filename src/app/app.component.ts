import { Router } from '@angular/router';
import { Component, OnInit, ViewContainerRef, ViewChild } from '@angular/core';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { environment } from 'environments/environment.prod';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  @ViewChild(ToastContainerDirective) toastContainer: ToastContainerDirective;

  title = 'app works!';

  public viewContainerRef: ViewContainerRef;

  public constructor(private toastrService: ToastrService, private router: Router) {

    toastrService.overlayContainer = this.toastContainer;
  }

  ngOnInit() {
    // this.router.navigate(['/login']);
  }
}
