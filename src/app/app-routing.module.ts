import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { AuthenticationComponent } from './authentication/authentication.component';

export const routes: Routes = [
  { path: 'login', loadChildren: './authentication/authentication.module#AuthenticationModule' },
  { path: 'app', loadChildren: './dashboard/dashboard.module#DashboardModule' },
  { path: '**', redirectTo: 'app', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule { }

export const routedComponents = [
  DashboardComponent,
  AuthenticationComponent
];
