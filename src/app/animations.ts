import {
  trigger,
  animate,
  transition,
  style,
  query
} from '@angular/animations';

export const fadeAnimation = trigger('fadeAnimation', [
  transition('* <=> *', [
    style({ position: 'relative' }),
    query(
      ':enter',
      [
        style({
          opacity: 0,
          transform: 'translateY(15px)',
          position: 'absolute'
        })
      ],
      {
        optional: true
      }
    ),
    query(
      ':leave',
      [
        style({ opacity: 1 }),
        animate('0.4s ease-out', style({ opacity: 0, position: 'absolute' }))
      ],
      { optional: true }
    ),
    query(
      ':enter',
      [
        style({ opacity: 0 }),
        animate(
          '0.4s ease-out',
          style({ opacity: 1, transform: 'translateY(0)' })
        )
      ],
      { optional: true }
    )
  ])
]);
