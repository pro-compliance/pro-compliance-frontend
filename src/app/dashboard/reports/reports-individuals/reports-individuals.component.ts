import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Participant } from '../../../shared/models/participants.model';
import { ParticipantsService } from '../../../shared/services/participants.service';
import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';

@Component({
  selector: 'reports-individuals',
  templateUrl: './reports-individuals.component.html',
  styleUrls: ['./reports-individuals.component.sass']
})
export class ReportsIndividualsComponent implements OnInit {
  _individuals: Observable<Participant[]>;
  _table: TableOptions = {};

  constructor(private _partServ: ParticipantsService) {}

  ngOnInit() {
    this._individuals = this._partServ
      .getParticipants('individuals')
      .map(response => {
        return response.data;
      });

    this._table.columns = [
      {
        name: 'fullName',
        title: 'Nombre',
        type: 'text',
        filterable: true,
        sortable: true
      },
      { name: 'code', title: '# Doc', filterable: true },
      { name: 'birthDate', title: 'Fec. Nac', type: 'date', sortable: true },
      {
        name: 'email',
        title: 'Email',
        type: 'text',
        sortable: true,
        filterable: true
      },
      {
        name: 'country',
        title: 'País',
        type: 'object',
        objectColumn: 'country.name',
        sortable: true,
        lookup: true
      },
      { name: 'score', title: 'Puntaje', type: 'decimal', sortable: true },
      {
        name: 'rate',
        title: 'Riesgo',
        type: 'text',
        sortable: true,
        lookup: true
      },
      { name: 'address', title: 'Dirección', hidden: true },
      {
        name: 'gender',
        title: 'Género',
        type: 'object',
        objectColumn: 'gender.name',
        hidden: true
      },
      { name: 'phone', title: 'Teléfono', hidden: true },
      { name: 'mobilePhone', title: 'Tel. Celular', hidden: true },
      { name: 'createDate', title: 'Fec. Creación', hidden: true }
    ];

    this._table.style = 'table  table-squared';

    this._table.pageable = true;

    this._table.searcheable = true;
    this._table.exportToCSV = true;
    this._table.exportToPDF = true;
    this._table.reportsOnly = true;
    this._table.title = 'Individuos';
  }
}
