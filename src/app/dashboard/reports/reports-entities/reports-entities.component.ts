import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Participant } from '../../../shared/models/participants.model';
import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';
import { ParticipantsService } from '../../../shared/services/participants.service';

@Component({
  selector: 'reports-entities',
  templateUrl: './reports-entities.component.html',
  styleUrls: ['./reports-entities.component.sass']
})
export class ReportsEntitiesComponent implements OnInit {
  _entities: Observable<Participant[]>;
  _table: TableOptions = {};

  constructor(private _partServ: ParticipantsService) {}

  ngOnInit() {
    this._entities = this._partServ
      .getParticipants('entities')
      .map(response => {
        return response.data;
      });

    this._table.columns = [
      {
        name: 'fullName',
        title: 'Razón Social',
        type: 'text',
        filterable: true,
        sortable: true
      },
      { name: 'code', title: 'RUC/NIT', sortable: true },
      {
        name: 'birthDate',
        title: 'Fec. Constitución',
        type: 'date',
        sortable: true
      },
      {
        name: 'email',
        title: 'Email',
        type: 'text',
        sortable: true,
        filterable: true
      },
      {
        name: 'country',
        title: 'País',
        type: 'object',
        objectColumn: 'country.name',
        sortable: true,
        lookup: true
      },
      { name: 'score', title: 'Puntaje', type: 'decimal', sortable: true },
      {
        name: 'rate',
        title: 'Riesgo',
        type: 'text',
        sortable: true,
        lookup: true
      },
      { name: 'address', title: 'Dirección', hidden: true },
      { name: 'webSite', title: 'Sitio Web', hidden: true },
      {
        name: 'legalRepresentative',
        title: 'Representante Legal',
        hidden: true
      },
      { name: 'phone', title: 'Teléfono', hidden: true },
      { name: 'mobilePhone', title: 'Tel. Celular', hidden: true },
      { name: 'createDate', title: 'Fec. Creación', hidden: true }
    ];

    this._table.style = 'table  table-squared';

    this._table.reportsOnly = true;

    this._table.pageable = true;

    this._table.exportToCSV = true;
    this._table.exportToPDF = true;

    this._table.searcheable = true;

    this._table.title = 'Entidades';
  }
}
