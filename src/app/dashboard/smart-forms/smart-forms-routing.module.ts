import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SmartFormsComponent } from './smart-forms.component';
import { SmartFormsListComponent } from './smart-forms-list/smart-forms-list.component';
import { SmartFormsEditComponent } from './smart-forms-edit/smart-forms-edit.component';

const routes: Routes = [
  {
    path: '',
    component: SmartFormsComponent,
    children: [
      { path: '', component: SmartFormsListComponent },
      { path: ':id', component: SmartFormsEditComponent }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class SmartFormsRoutingModule {}
