import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SmartFormsComponent } from './smart-forms.component';
import { SmartFormsRoutingModule } from './smart-forms-routing.module';
import { SmartFormsListComponent } from './smart-forms-list/smart-forms-list.component';
import { SharedModule } from 'app/shared/shared.module';
import { SmartFormsEditComponent } from './smart-forms-edit/smart-forms-edit.component';
import { FormFieldComponent } from './form-field/form-field.component';

@NgModule({
  imports: [CommonModule, SmartFormsRoutingModule, SharedModule],
  declarations: [SmartFormsComponent, SmartFormsListComponent, SmartFormsEditComponent, FormFieldComponent]
})
export class SmartFormsModule {}
