import { SmartFormsModule } from './smart-forms.module';

describe('SmartFormsModule', () => {
  let smartFormsModule: SmartFormsModule;

  beforeEach(() => {
    smartFormsModule = new SmartFormsModule();
  });

  it('should create an instance', () => {
    expect(smartFormsModule).toBeTruthy();
  });
});
