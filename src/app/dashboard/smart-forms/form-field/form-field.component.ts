import { Component, OnInit, Input } from '@angular/core';
import { FormField, FieldType } from 'app/shared/models/smart-forms.models';
import { FormFieldsService } from 'app/shared/services/form-fields.service';
import { Observable } from 'rxjs';
import { FieldTypesService } from 'app/shared/services/field-types.service';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.sass']
})
export class FormFieldComponent implements OnInit {
  @Input() field: FormField;

  _currentField: FormField = {};
  _types: Observable<FieldType[]>;

  constructor(
    private _fieldService: FormFieldsService,
    private _typesService: FieldTypesService
  ) {}

  ngOnInit() {
    this._types = this._typesService.getTypes().map(response => {return response.data});
    if (this.field) {
      this._currentField = Object.assign(this._currentField, this.field);
      console.log(this._currentField);
    }
  }
}
