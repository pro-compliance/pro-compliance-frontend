import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartFormsEditComponent } from './smart-forms-edit.component';

describe('SmartFormsEditComponent', () => {
  let component: SmartFormsEditComponent;
  let fixture: ComponentFixture<SmartFormsEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartFormsEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartFormsEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
