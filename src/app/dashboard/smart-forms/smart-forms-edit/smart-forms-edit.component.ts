import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SmartFormsService } from 'app/shared/services/smart-forms.service';
import { SmartForm } from 'app/shared/models/smart-forms.models';

@Component({
  selector: 'app-smart-forms-edit',
  templateUrl: './smart-forms-edit.component.html',
  styleUrls: ['./smart-forms-edit.component.sass']
})
export class SmartFormsEditComponent implements OnInit {
  public _currentForm: SmartForm;

  constructor(
    private router: ActivatedRoute,
    private _formService: SmartFormsService
  ) {}

  ngOnInit() {
    this.router.params.subscribe(params => {
      this._formService.getForm(params['id']).subscribe(response => {
        this._currentForm = response.data;
      });
    });
  }
}
