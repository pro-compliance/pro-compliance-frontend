import { Component, OnInit } from '@angular/core';
import { SmartFormsService } from 'app/shared/services/smart-forms.service';
import { Observable } from 'rxjs';
import { SmartForm } from 'app/shared/models/smart-forms.models';

@Component({
  selector: 'app-smart-forms-list',
  templateUrl: './smart-forms-list.component.html',
  styleUrls: ['./smart-forms-list.component.sass']
})
export class SmartFormsListComponent implements OnInit {

  _forms: Observable<SmartForm[]>;

  constructor(private _formsService: SmartFormsService) { }

  ngOnInit() {
    this._forms = this._formsService.getForms().map(response => {return response.data});
  }

}
