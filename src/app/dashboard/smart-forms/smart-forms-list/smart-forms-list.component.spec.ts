import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmartFormsListComponent } from './smart-forms-list.component';

describe('SmartFormsListComponent', () => {
  let component: SmartFormsListComponent;
  let fixture: ComponentFixture<SmartFormsListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmartFormsListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmartFormsListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
