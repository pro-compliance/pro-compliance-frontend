import { Component, OnInit, ViewChild } from '@angular/core';
import {
  NgbModal,
  NgbDateStruct,
  NgbActiveModal
} from '@ng-bootstrap/ng-bootstrap';
import { RoadmapService } from '../../../shared/services/roadmap.service';
import { Roadmap } from '../../../shared/models/roadmap.model';
import { ToastrService } from 'ngx-toastr';
import { RoadmapsFormComponent } from '../roadmaps-form/roadmaps-form.component';

@Component({
  selector: 'app-roadmap',
  templateUrl: './roadmap.component.html',
  styleUrls: ['./roadmap.component.sass']
})
export class RoadmapComponent implements OnInit {
  @ViewChild(RoadmapsFormComponent) private form: RoadmapsFormComponent;

  _roadMaps: Roadmap[];
  _newRoadmap: Roadmap;
  _selectedRoadmap: Roadmap = {};
  _currentRoadMap: Roadmap;
  _startDate: NgbDateStruct;
  _endDate: NgbDateStruct;
  closeResult: string;
  _default: undefined;

  constructor(
    private _roadmapServ: RoadmapService,
    private modal: NgbModal,
    private toast: ToastrService,
    public activeModal: NgbActiveModal
  ) {}

  ngOnInit() {
    this._roadmapServ.getRoadmaps().subscribe(response => {
      this._roadMaps = response.data;
    });
  }

  open() {
    const modalRef = this.modal.open(RoadmapsFormComponent);
    modalRef.result.then(
      result => {
        this.save();
      },
      dismiss => {
        this._selectedRoadmap = {};
      }
    );
    modalRef.componentInstance.currentRoadmap = this._selectedRoadmap;
  }

  save() {
    if (!this._selectedRoadmap.ID) {
      this.createRoadmap();
    }
  }
  selectRoadmap() {
    this._selectedRoadmap = Object.assign(
      {},
      this._selectedRoadmap,
      this._currentRoadMap
    );
    this.open();
  }

  createRoadmap() {
    this._roadmapServ
      .createRoadmap(this._selectedRoadmap)
      .subscribe(response => {
        this._roadMaps.push(response.data);
        this.toast.success('Cronograma creado exitosamente!');
      });
  }

  updateRoadmap() {
    this._roadmapServ
      .updateRoadmap(this._selectedRoadmap.ID, this._selectedRoadmap)
      .subscribe(response => {
        this._currentRoadMap = this._selectedRoadmap;
        this.toast.success('Cronograma editado exitosamente!');
      });
  }
}
