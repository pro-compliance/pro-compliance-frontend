import { Component, OnInit } from '@angular/core';
import { ProjectsService } from '../../../shared/services/projects.service';
import { Project } from '../../../shared/models/assignment.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  styleUrls: ['./projects.component.sass']
})
export class ProjectsComponent implements OnInit {
  _projects: Project[];
  _projectTitle: string;
  constructor(private _projectServ: ProjectsService, private toast: ToastrService) {}

  ngOnInit() {
    this._projectServ.getProjects().subscribe(response => {
      this._projects = response.data;
    });
  }

  onKeyDown(event) {
    if (event.key === 'Enter') {
      this.addProject();
    }
  }

  addProject() {
    const _newProject: Project = {
      title: this._projectTitle,
      active: true
    };
    this._projectServ.createProject(_newProject).subscribe(response => {
      this.toast.success('Proyecto creado!', response.data.title);
      this._projectTitle = '';
      this._projects.push(response.data);
    });
  }
}
