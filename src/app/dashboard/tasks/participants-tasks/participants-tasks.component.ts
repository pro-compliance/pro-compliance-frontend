import { Component, OnInit } from '@angular/core';
import { Task } from '../../../shared/models/tasks.model';
import { Participant } from '../../../shared/models/participants.model';
import { TasksService } from '../../../shared/services/tasks.service';
import { UtilService } from '../../../shared/services/util.service';

@Component({
  selector: 'participants-tasks',
  templateUrl: './participants-tasks.component.html',
  styleUrls: ['./participants-tasks.component.sass']
})
export class ParticipantsTasksComponent implements OnInit {
  _tasks: Task[];
  _participants: Participant[];
  _view: string;

  constructor(
    private _taskServ: TasksService,
    private _util: UtilService
  ) {
    this._view = 'alpha';
  }

  ngOnInit() {
    this._taskServ.getTaksByParticipant().subscribe(response => {
      this._participants = this._util.sortBy(response.data, 'ShortName');
    });
  }
}
