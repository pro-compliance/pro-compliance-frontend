import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Participant } from '../../../shared/models/participants.model';
import { Gender } from '../../../shared/models/genders.model';
import { Country } from '../../../shared/models/country.model';
import { MapsService } from '../../../shared/services/maps.service';
import { CountriesService } from '../../../shared/services/countries.service';
import { ParticipantsService } from '../../../shared/services/participants.service';
import { UtilService } from '../../../shared/services/util.service';
import { ParamMatrix } from '../../../shared/models/params.model';
import { ParamMatricesService } from '../../../shared/services/param-matrices.service';

const NOW = new Date();

@Component({
  selector: 'individual-form',
  templateUrl: './individual-form.component.html',
  styleUrls: ['./individual-form.component.sass']
})
export class IndividualFormComponent implements OnInit {
  @Input() individual?: Participant;
  @Output() updateParticipant = new EventEmitter();
  _saving: boolean;
  _individual: Participant;
  _genders: Gender[];
  _default: any = undefined;
  _startDate: any;
  _maxDate: any;
  _minDate: any;
  _countries: Country[];
  _location: any;
  _matrices: Observable<ParamMatrix[]>;

  constructor(
    private _partServ: ParticipantsService,
    private _util: UtilService,
    private _countryServ: CountriesService,
    private _map: MapsService,
    private _paramService: ParamMatricesService,
    private toastr: ToastrService,
    private _router: Router
  ) {
    this._genders = [
      {
        id: 1,
        name: 'Femenino',
        englishName: 'Female'
      },
      {
        id: 2,
        name: 'Masculino',
        englishName: 'Male'
      }
    ];

    this._startDate = {
      year: NOW.getFullYear() - 18,
      month: 1
    };

    this._maxDate = {
      year: NOW.getFullYear() - 18,
      month: 12,
      day: 31
    };

    this._minDate = {
      year: NOW.getFullYear() - 100,
      month: 1,
      day: 1
    };
    this._saving = false;
  }

  ngOnInit() {

    this._matrices = this._paramService.getMatrices().map(response => {return response.data});
    this._countryServ.getCountries().subscribe(response => {
      this._countries = this._util.sortBy(response.data, 'name');
    });
    if (!this.individual) {
      this._individual = {
        participantTypeId: 1
      };
    } else {
      this._individual = this.individual;
      this.setLocation();
    }
  }

  setLocation() {
    this._map.getPosition(this._individual.address).subscribe(position => {
      this._location = position.results[0].geometry.location;
    });
  }
  saveIndividual() {
    this._saving = true;
    if (!this.individual) {
      this._partServ.createParticipant(this._individual).subscribe(response => {
        this.toastr.success(response.data.shortName, 'Individuo Creado');
        this._router.navigate(['app/participantes', response.data.id]);
      });
    } else {
      this._individual.country = this._util.filterByID(this._countries, this._individual.countryId);
      this._partServ.updateParticipant(this._individual.id, this._individual).subscribe(response => {
        this.toastr.success(response.data.shortName, 'Individuo Actualizado');
        this.updateParticipant.emit();
      });
    }
  }
}
