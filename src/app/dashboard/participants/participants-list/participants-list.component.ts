import { Component, OnInit } from '@angular/core';
import { Participant } from '../../../shared/models/participants.model';
import { ParticipantsService } from '../../../shared/services/participants.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'participants-list',
  templateUrl: './participants-list.component.html',
  styleUrls: ['./participants-list.component.sass']
})
export class ParticipantsListComponent implements OnInit {

  _participants: Participant[];
  _entities: Participant[];
  _individuals: Participant[];
  _filter: any[] = [];

  constructor(
    private _partServ: ParticipantsService,
    private title: Title
  ) { }

  ngOnInit() {
    this.title.setTitle('ProCompliance | Listado de Participantes')
    this._partServ.getParticipants()
      .subscribe(response => {
        this._participants = response.data;
        this.classify();
      });
  }

  classify() {
    this._entities = this._participants.filter(item => item.participantTypeId === 2);
    this._individuals = this._participants.filter(item => item.participantTypeId === 1);
  }

}
