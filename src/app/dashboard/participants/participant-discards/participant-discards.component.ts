import { ToastrService } from 'ngx-toastr';
import { ParticipantDiscard } from './../../../shared/models/discards.models';
import { Component, OnInit, Input } from '@angular/core';
import { DiscardsService } from '../../../shared/services/discards.service';
import { Observable } from 'rxjs/Observable';
import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';

@Component({
  selector: 'app-participant-discards',
  templateUrl: './participant-discards.component.html',
  styleUrls: ['./participant-discards.component.sass']
})
export class ParticipantDiscardsComponent implements OnInit {
  @Input()
  participantId: number;

  _discards: Observable<ParticipantDiscard[]>;
  _table: TableOptions = {};

  constructor(
    private _discardServ: DiscardsService,
    private _toast: ToastrService
  ) {}

  ngOnInit() {
    this._discards = this._discardServ
      .getParticipantDiscards(this.participantId)
      .map(response => {
        return response.data;
      });
    this._table.style = '';
    this._table.columns = [
      {
        name: 'sanctionList',
        title: 'Lista',
        type: 'object',
        objectColumn: 'sanctionList.name',
        sortable: true,
        listDisplay: 'name',
        listID: 'id',
        objectID: 'sanctionListId'
      },
      {
        name: 'date',
        title: 'Fecha',
        type: 'datetime'
      },
      { name: 'match', title: 'Coincidencias', type: 'boolean' }
    ];
  }

  runDiscard() {
    this._discardServ
      .runParticipantDiscards(this.participantId)
      .subscribe(response => {
        this._toast.success('Discarte ejecutado exitosamente');
        this._discards = this._discardServ
          .getParticipantDiscards(this.participantId)
          .map(responsed => {
            return responsed.data;
          });
      });
  }
}
