import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ParticipantDiscardsComponent } from './participant-discards.component';

describe('ParticipantDiscardsComponent', () => {
  let component: ParticipantDiscardsComponent;
  let fixture: ComponentFixture<ParticipantDiscardsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ParticipantDiscardsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ParticipantDiscardsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
