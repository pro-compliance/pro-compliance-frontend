import { Observable } from 'rxjs';
import { Component, OnInit, Input } from '@angular/core';

import { ParticipantsService } from '../../../shared/services/participants.service';
import {
  PendingDocument,
  Participant
} from '../../../shared/models/participants.model';

@Component({
  selector: 'participant-tasks',
  templateUrl: './participant-tasks.component.html',
  styleUrls: ['./participant-tasks.component.sass']
})
export class ParticipantTasksComponent implements OnInit {
  @Input()
  participant: Participant;

  _pending: Observable<PendingDocument[]>;
  constructor(private _partServ: ParticipantsService) {}

  ngOnInit() {
    this._pending = this._partServ
      .getPendingDocuments(this.participant.id)
      .map(response => {
        return response.data;
      });
  }
}
