import { Component, OnInit } from '@angular/core';

import { ParticipantType } from '../../../shared/models/participants.model';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'participant-form',
  templateUrl: './participant-form.component.html',
  styleUrls: ['./participant-form.component.sass']
})
export class ParticipantFormComponent implements OnInit {
  _types: ParticipantType[];
  _type: ParticipantType;
  _default: any = undefined;

  constructor(private title: Title) {
    this._types = [
      {
        id: 1,
        englishName: 'Individual',
        name: 'Individuo'
      },
      {
        id: 2,
        englishName: 'Entity',
        name: 'Entidad'
      }
    ];
  }

  ngOnInit() {
    this.title.setTitle('ProCompliance | Nuevo Participante');
  }
}
