import { Component, OnInit } from '@angular/core';
import { fadeAnimation } from 'app/animations';

@Component({
  selector: 'app-participants',
  templateUrl: './participants.component.html',
  styleUrls: ['./participants.component.sass'],
  animations: [fadeAnimation]
})
export class ParticipantsComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
