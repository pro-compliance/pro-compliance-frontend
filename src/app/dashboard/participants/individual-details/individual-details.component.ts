import { Component, OnInit, Input } from '@angular/core';

import { Participant } from '../../../shared/models/participants.model';
import { MapsService } from '../../../shared/services/maps.service';
import { ParticipantsService } from 'app/shared/services/participants.service';

@Component({
  selector: 'individual-details',
  templateUrl: './individual-details.component.html',
  styleUrls: ['./individual-details.component.sass']
})
export class IndividualDetailsComponent implements OnInit {
  @Input() individual: Participant;
  position: any;
  lng: number;
  lat: number;
  _values: number[];
  percent: number;
  _labels = ['Completos', 'Pendientes'];

  constructor(
    private _map: MapsService,
    private _participantService: ParticipantsService
  ) {}

  ngOnInit() {
    if (this.individual.address) {
      this._map.getPosition(this.individual.address).subscribe(position => {
        this.position = position.results[0].geometry.location;
      });
    }

    this._participantService
      .getDiligence(this.individual.id)
      .subscribe(response => {
        this._values = response.data;
      });
  }

  percentProgress() {}
}
