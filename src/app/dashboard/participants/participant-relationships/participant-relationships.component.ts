import {
  Component,
  OnInit,
  Input,
  OnChanges,
  SimpleChanges
} from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { ToastrService } from 'ngx-toastr';
import { ParticipantRelationship } from '../../../shared/models/relationships.model';
import { Participant } from '../../../shared/models/participants.model';
import { RelationshipsService } from '../../../shared/services/relationships.service';
import { UtilService } from '../../../shared/services/util.service';
import { ParticipantRelationshipComponent } from '../participant-relationship/participant-relationship.component';

@Component({
  selector: 'participant-relationships',
  templateUrl: './participant-relationships.component.html',
  styleUrls: ['./participant-relationships.component.sass']
})
export class ParticipantRelationshipsComponent implements OnInit, OnChanges {
  @Input() participant: Participant;

  _currentRelationship: ParticipantRelationship = {};
  _relationships: ParticipantRelationship[];

  closeResult: string;

  constructor(
    private modalService: NgbModal,
    private _relService: RelationshipsService,
    private _util: UtilService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this._currentRelationship.participant = this.participant;
  }

  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (propName === 'participant') {
        this.getRelationships();
      }
    }
  }

  getRelationships() {
    this._relService
      .getRelationships(this.participant.id)
      .subscribe(response => {
        this._relationships = response.data;
      });
  }

  open() {
    const modalRef = this.modalService.open(ParticipantRelationshipComponent);

    modalRef.result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
        this.addRelationShip();
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );

    modalRef.componentInstance.relation = this._currentRelationship;
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  addRelationShip() {
    const relationship: ParticipantRelationship = {};

    relationship.relationshipTypeId = this._currentRelationship.type.id;
    relationship.participantId = this._currentRelationship.participant.id;
    relationship.relatedParticipantId = this._currentRelationship.relatedParticipant.id;
    this._relService.addRelationship(relationship).subscribe(response => {
      console.log(response);
      this.toastr.success(
        response.data.relatedParticipant.shortName,
        'Relación agregada'
      );
      this._relationships.push(response.data);
    });
  }

  removeRelation(id: number) {
    this._relService.deleteRelationship(id).subscribe(response => {
      this.toastr.info('Relación eliminada');
      this._relationships = this._util.removeByID(this._relationships, id);
    });
  }
}
