import { Component, OnInit } from '@angular/core';
import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';
import { AccountType } from '../../../shared/models/profiles.model';
import { ProfileAccountsService } from '../../../shared/services/profile-accounts.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-account-types',
  templateUrl: './account-types.component.html',
  styleUrls: ['./account-types.component.sass']
})
export class AccountTypesComponent implements OnInit {
  _table: TableOptions = {};
  _types: AccountType[];

  constructor(private _typeServ: ProfileAccountsService, private toast: ToastrService) {}

  ngOnInit() {
    this._table.title = 'Tipos Producto';
    this._table.pageable = true;
    this._table.editable = true;
    this._table.addMethod = 'modal';
    this._table.searcheable = true;
    this._table.style = 'table-squared';
    this._table.deletable = true;
    this._table.columns = [
      { name: 'id', title: '#', hidden: true },
      { name: 'name', title: 'Nombre', filterable: true },
      { name: 'englishName', title: 'Nombre Inglés', filterable: true }
    ];

    this._typeServ.getTypes().subscribe(response => {
      this._types = response.data;
    });
  }

  addType(type: AccountType) {
    this._typeServ.addType(type).subscribe(
      response => {
        this.toast.success(response.data.name, 'Producto agregada');
        this._types.push(response.data);
      },
      (err: Error) => {
        this.toast.error(err.message, 'Ocurrió un error');
      }
    );
  }

  editType(type: AccountType) {
    this._typeServ.editType(type.id, type).subscribe(
      response => {
        this.toast.success(type.name, 'Producto actualizada');
      },
      (err: Error) => {
        this.toast.error(err.message, 'Ocurrió un error');
      }
    );
  }

  deleteType(id: number) {
    this._typeServ.deleteType(id).subscribe(
      response => {
        this.toast.info('Producto eliminado');
      },
      (err: Error) => {
        this.toast.error(err.message, 'Ocurrión un error');
      }
    );
  }
}
