import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { CountriesService } from '../../../shared/services/countries.service';
import { Country } from '../../../shared/models/country.model';
import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.component.html',
  styleUrls: ['./countries.component.sass']
})
export class CountriesComponent implements OnInit {
  _options: TableOptions = {};
  _countries: Observable<Country[]>;

  constructor(
    private _countryServ: CountriesService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this._options.columns = [
      { name: 'name', title: 'Nombre', filterable: true, required: true },
      {
        name: 'englishName',
        title: 'Nombre Inglés',
        filterable: true,
        required: true
      },
      { name: 'abbreviation', title: 'Código', filterable: true },
      {
        name: 'blackList',
        title: 'Lista Negra',
        type: 'boolean',
        required: true
      }
    ];

    this._options.style = 'table  table-squared';

    this._options.pageable = true;

    this._options.title = 'Países';

    this._options.searcheable = true;

    this._options.exportToCSV = true;

    this._options.editable = true;

    this._options.addMethod = 'modal';

    this._countries = this._countryServ.getCountries().map(response => {
      return response.data;
    });
  }

  addCountry(country: Country) {
    return this._countryServ.addCountry(country).subscribe(response => {
      this.toastr.success(response.data.name, 'País añadido');
      this._countries = this._countryServ.getCountries().map(responsed => {
        return responsed.data;
      });
    });
  }

  updateCountry(country: Country) {
    this._countryServ.editCountry(country.id, country).subscribe(response => {
      this.toastr.success(country.name, 'País editado');
      this._countries = this._countryServ.getCountries().map(responsed => {
        return responsed.data;
      });
    });
  }
}
