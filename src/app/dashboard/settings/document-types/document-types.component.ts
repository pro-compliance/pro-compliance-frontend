import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';
import { DocumentType } from '../../../shared/models/documents.models';
import { DocumentsService } from '../../../shared/services/documents.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-document-types',
  templateUrl: './document-types.component.html',
  styleUrls: ['./document-types.component.sass']
})
export class DocumentTypesComponent implements OnInit {
  _types: Observable<DocumentType[]>;
  _currentType: DocumentType = {};
  _newType: DocumentType = {};
  _table: TableOptions = {};

  constructor(
    private _docServ: DocumentsService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this._table.columns = [
      { name: 'name', title: 'Nombre', sortable: true, required: true },
      {
        name: 'englishName',
        title: 'Nombre Inglés',
        sortable: true,
        required: true
      },
      {
        name: 'requiredIndividual',
        title: 'Req. Individuo',
        type: 'boolean',
        required: true
      },
      {
        name: 'requiredEntity',
        title: 'Req. Entidad',
        type: 'boolean',
        required: true
      }
    ];

    this._table.editable = true;
    this._table.deletable = true;
    this._table.style = ' table-squared';
    this._table.addMethod = 'modal';
    this._table.pageable = true;
    this._table.exportToCSV = true;
    this._types = this._docServ.getTypes().map(response => {
      return response.data;
    });
  }

  selectType(type: DocumentType) {
    this._currentType = type;
  }

  cancelUpdate() {
    this._currentType = {};
  }

  addType(type: DocumentType) {
    this._docServ.addType(type).subscribe(response => {
      this.toastr.success(response.data.name, 'Tipo Documento Creado');
      this._types = this._docServ.getTypes().map(responsed => {
        return responsed.data;
      });
    });
  }

  deleteType(id: number) {
    this._docServ.deleteType(id).subscribe(data => {
      this.toastr.info('Tipo de documento eliminado');
    });
  }

  updateType(type: DocumentType) {
    this._docServ.updateType(type.id, type).subscribe(data => {
      this.toastr.success(type.name, 'Tipo Documento Editado');
      this._types = this._docServ.getTypes().map(response => {
        return response.data;
      });
    });
  }
}
