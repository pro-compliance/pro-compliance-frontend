import { Component, OnInit } from '@angular/core';

import { ParticipantsService } from '../../shared/services/participants.service';
import { MapsService } from '../../shared/services/maps.service';
import { UtilService } from '../../shared/services/util.service';
import { Participant } from '../../shared/models/participants.model';
import { Title } from '@angular/platform-browser';
import { Observable } from 'rxjs';

interface Part extends Participant {
  location?: any;
}

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  public byRisk: any[];
  public byCountry: any[];
  public byType: any[];
  public _countries: any;
  public _lastParticipants: Participant[];
  public _participants: Part[];
  public _mapReady: boolean;
  public _count: number;

  public riskChartOptions: any;
  public countryChartOptions: any;
  public tasksChartOptions: any;
  public _tasks: any[];
  public _partTasks: any[];
  public riskColors: any[];
  public chartReady = false;
  public _addresses: any[];

  constructor(
    private _partServ: ParticipantsService,
    private _util: UtilService,
    private _map: MapsService,
    private title: Title
  ) {
    _partServ.getParticipants().subscribe(response => {
      this._addresses = [];
      this._participants = <Participant[]>response.data;
      this.byType = (_util.mapCountItems(this._participants, 'type.name'));
      this._participants.forEach(part => {
        let location: any = {};
        _map.getPosition(part.address).subscribe(position => {
          if (position.results[0]) {
            location = position.results[0];
            location.participant = part;
            this._addresses.push(location);
            part.location = location;
          }
        });
      });
    });
  }

  ngOnInit() {
    this.title.setTitle('ProCompliance | Inicio')
    this._partServ.getParticipantsbyRisk().subscribe(response => {
      this.byRisk = <Participant[]>response.data;
      for (const i of this.byRisk) {
        if (i.rate === 'No disponible') {
          i.name = 'No disponible';
        } else {
          i.name = `${i.rate} Riesgo`;
        }
      }
      this.byRisk = this._util.sortBy(this.byRisk, 'sort', true);
      this.loadRiskChart();
    });
    this.loadCountry();
    this.loadTasks();
    this.loadLastParticipants();
    this._partServ.getCount().subscribe(response => {this._count = response.data});
  }

  loadRiskChart() {
    this.riskChartOptions = {
      title: {
        display: false,
        text: 'Distribución de Riesgo',
        fontFamily: 'Niramit',
        fontSize: 12
      },
      legend: {
        position: 'left',
        labels: {
          fontFamily: 'Niramit',
          boxWidth: 15,
          fontSize: 12,
          fontStyle: 'bold'
        }
      },
      tooltips: {
        bodyFontFamily: 'Niramit',
        bodyFontSize: 14
      }
    };
    this.riskColors = [
      {
        backgroundColor: ['#db7b7b', '#f8cd79', '#639a6f', '#b2b2b2']
      }
    ];
  }

  loadTasks() {
    this.tasksChartOptions = {
      title: {
        display: false,
        text: 'Tareas Diarias',
        fontFamily: 'Niramit',
        fontSize: 14
      },
      legend: {
        position: 'left',
        labels: {
          fontFamily: 'Niramit',
          boxWidth: 15,
          fontSize: 12,
          fontStyle: 'bold'
        }
      },
      tooltips: {
        bodyFontFamily: 'Niramit',
        bodyFontSize: 12
      }
    };
  }

  loadCountry() {
    this.countryChartOptions = {
      title: { display: true, text: 'Participantes por País', fontFamily: 'Niramit', fontSize: 16 },
      legend: { position: 'left', labels: { fontFamily: 'Niramit', boxWidth: 20, fontStyle: 'bold' } },
      tooltips: { bodyFontFamily: 'Niramit', bodyFontSize: 14 }
    };
    this._partServ.getParticipantsbyCountry().subscribe(response => {
      this.byCountry = response.data;
      this._countries = {};
      this.byCountry.forEach(country => {
        this._countries[country.abbreviation.toLowerCase()] = country.value;
      });
    });
  }

  loadLastParticipants() {
    this._partServ.getLastParticipants().subscribe(response => {
      this._lastParticipants = <Participant[]>response.data;
    });
  }
}
