import { AuthService } from './../shared/services/auth.service';
import { Component, OnInit, HostBinding } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { DocumentsService } from '../shared/services/documents.service';
import { fadeAnimation } from 'app/animations';
import { RouterOutlet } from '@angular/router';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.sass'],
  animations: [fadeAnimation]
})
export class DashboardComponent implements OnInit {
  @HostBinding('@fadeAnimation') fadeAnimation = true;
  @HostBinding('style.display') display = 'block';
  @HostBinding('style.position') position = 'relative';
  _active = false;
  constructor(
    private toast: ToastrService,
    private _authServ: AuthService,
    private _docServ: DocumentsService
  ) {}

  ngOnInit() {}

  toogleSidebar() {
    this._active = !this._active;
  }

  prepareRoute(outlet: RouterOutlet) {
    console.log(outlet.activatedRouteData);
    console.log(outlet.activatedRouteData['animation']);
    return outlet && outlet.activatedRouteData && outlet.activatedRouteData['animation'];
  }
}
