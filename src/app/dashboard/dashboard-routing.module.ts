import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { HomeComponent } from './home/home.component';
import { AuthGuard } from '../shared/services/auth.guard';
import { SettingsComponent } from './settings/settings.component';
import { SmartFormsComponent } from './smart-forms/smart-forms.component';

export const routes: Routes = [
  {
    path: '',
    component: DashboardComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        canActivateChild: [AuthGuard],
        component: HomeComponent,
        data: {animation: 'HomePage'}
      },
      {
        path: 'participantes',
        canActivateChild: [AuthGuard],
        loadChildren: './participants/participants.module#ParticipantsModule'
      },
      {
        path: 'tareas',
        canActivateChild: [AuthGuard],
        loadChildren: './tasks/tasks.module#TasksModule'
      },
      {
        path: 'parametros',
        canActivateChild: [AuthGuard],
        loadChildren: './params/params.module#ParamsModule'
      },
      {
        path: 'usuarios',
        canActivateChild: [AuthGuard],
        loadChildren: './users/users.module#UsersModule'
      },
      {
        path: 'training',
        canActivateChild: [AuthGuard],
        loadChildren: './training/training.module#TrainingModule'
      },
      {
        path: 'reportes',
        canActivateChild: [AuthGuard],
        loadChildren: './reports/reports.module#ReportsModule'
      },
      {
        path: 'alertas',
        canActivateChild: [AuthGuard],
        loadChildren: './alerts/alerts.module#AlertsModule'
      },
      {
        path: 'descartes',
        canActivateChild: [AuthGuard],
        loadChildren: './discards/discards.module#DiscardsModule'
      },
      {
        path: 'ajustes',
        canActivateChild: [AuthGuard],
        component: SettingsComponent
      },
      {
        path: 'formularios',
        canActivateChild: [AuthGuard],
        loadChildren: './smart-forms/smart-forms.module#SmartFormsModule'
      },
      {
        path: '**',
        redirectTo: 'home',
        pathMatch: 'full'
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule {}

export const routedComponents = [DashboardComponent];
