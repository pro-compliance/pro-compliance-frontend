import { Component } from '@angular/core';
import { Location } from '@angular/common';
import { Router } from '@angular/router';

@Component({
  selector: 'app-breadcrumb',
  templateUrl: './breadcrumb.component.html',
  styleUrls: ['./breadcrumb.component.sass']
})
export class BreadcrumbComponent {
  routes: string[];
  path: string;
  isHome: boolean;

  constructor(private location: Location, private router: Router) {
    router.events.subscribe(val => {
      if (location.path() !== '' && location.path() !== this.path) {
        this.routes = [];
        this.path = location.path();
        this.path.split('/').forEach(e => {
          if (e !== '' && e !== 'home' && e !== 'app') {
            this.isHome = false;
            this.routes.push(e);
          } else {
            this.isHome = true;
          }
        });
      }
    });
  }

  back() {
    this.location.back();
  }
}
