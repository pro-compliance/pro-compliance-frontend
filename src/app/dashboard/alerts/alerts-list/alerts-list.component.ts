import { TableOptions } from './../../../shared/components/custom-table/custom-table.options';
import { Alert } from './../../../shared/models/alerts.model';
import { AlertsService } from './../../../shared/services/alerts.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-alerts-list',
  templateUrl: './alerts-list.component.html',
  styleUrls: ['./alerts-list.component.sass']
})
export class AlertsListComponent implements OnInit {
  _alerts: Observable<Alert[]>;
  _table: TableOptions = {};

  constructor(private _alertService: AlertsService) {}

  ngOnInit() {
    this._alerts = this._alertService
      .getAllAlerts()
      .map(response => response.data);
    this._table.pageable = true;
    this._table.columns = [
      {
        name: 'source',
        title: 'Fuente',
        sortable: true,
        type: 'object',
        objectColumn: 'source.name'
      },
      {
        name: 'reason',
        title: 'Generador',
        sortable: true,
        type: 'object',
        objectColumn: 'reason.name'
      },
      { name: 'description', title: 'Description' },
      {
        name: 'participant',
        title: 'Participante',
        sortable: true,
        type: 'object',
        objectColumn: 'participant.shortName'
      },
      { name: 'createDate', title: 'Fecha Alerta', type: 'datetime' }
    ];
  }
}
