import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-discards',
  templateUrl: './discards.component.html',
  styleUrls: ['./discards.component.sass']
})
export class DiscardsComponent implements OnInit {

  constructor(private title: Title) { }

  ngOnInit() {
    this.title.setTitle('ProCompliance | Sistema de Descartes');
  }

}
