import { DiscardsService } from './../../../shared/services/discards.service';
import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';

import { ComparisonsService } from '../../../shared/services/comparisons.service';
import { Comparison, Match } from '../../../shared/models/sanctions.model';

import * as XLSX from 'xlsx';

@Component({
  selector: 'discard-files',
  templateUrl: './files.component.html',
  styleUrls: ['./files.component.sass']
})
export class FilesComponent implements OnInit {
  data: any[];
  wopts: XLSX.WritingOptions = { bookType: 'xlsx', type: 'array' };
  closeResult: string;
  columns: any[];
  _firstRow: any;
  selectedCols: any[];
  _selectedItems: any[];
  _filename: string;
  _comparison: Comparison;
  _matches: any[];
  _searching = false;

  constructor(
    private toastr: ToastrService,
    private modalService: NgbModal,
    private _compService: ComparisonsService,
    private _discardsService: DiscardsService
  ) {}

  ngOnInit() {}

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>evt.target;
    if (target.files.length !== 1) {
      throw new Error('No se permiten múltiples archivos');
    }

    // const reader: FileReader = new FileReader();
    this._filename = target.files[0].name;

    this._discardsService.uploadExcel(target.files[0]).subscribe(response => {
      this.data = response.data;
      this.columns = [];
      this.selectedCols = [];

      this._firstRow = this.data[0];
      // tslint:disable-next-line:forin
      for (const key in this._firstRow) {
        const item: any = {};
        item.column = key;
        item.valid = false;
        this.columns.push(item);
      }
      this.toastr.info(`Archivo ${this._filename} cargado exitosamente!`);
    });
  }

  toggleColumn(name: string) {
    const column = this.columns.find(item => item.column === name);
    column.valid = !column.valid;
    this.selectedCols = [];
    this.columns.forEach(element => {
      if (element.valid) {
        const current: any = {};
        current.name = element.column;
        current.index = element.index;
        this.selectedCols.push(current);
      }
    });
  }

  open(content) {
    this.modalService.open(content).result.then(
      result => {
        this.closeResult = `Closed with: ${result}`;
        this.prepareDiscard();
      },
      reason => {
        this.closeResult = `Dismissed ${this.getDismissReason(reason)}`;
      }
    );
  }

  private getDismissReason(reason: any): string {
    if (reason === ModalDismissReasons.ESC) {
      return 'by pressing ESC';
    } else if (reason === ModalDismissReasons.BACKDROP_CLICK) {
      return 'by clicking on a backdrop';
    } else {
      return `with: ${reason}`;
    }
  }

  prepareDiscard() {
    this._searching = true;
    this._compService.addComparison(this._filename).subscribe(response => {
      this._comparison = response.data;
      this.runDiscard();
    });
  }

  getMatches(comparisonID: number) {
    this._compService
      .getMatchesbyComparison(comparisonID)
      .subscribe(data => {});
  }

  runDiscard() {
    this._selectedItems = [];
    this.data.forEach((row, index) => {
      if (index > 0) {
        const item: any = {};
        this.columns.forEach(col => {
          item[col.column] = row[col.index];
        });
        this._selectedItems.push(item);
      }
    });
    this._compService
      .runComparison(
        this._comparison.ID,
        this._selectedItems,
        this.selectedCols
      )
      .then(matches => {
        this._matches = matches;
        if (this._matches.length > 0) {
          this.saveMatches(this._matches.shift());
          this.toastr.success(
            `${matches.length} coincidencias`,
            'Comparación ejecutada'
          );
          this._searching = false;
        } else {
          this.toastr.success('0 coincidencias', 'Comparación ejecutada');
          this._searching = false;
        }
      });
  }

  saveMatches(match: any) {
    const current: Match = {};
    current.ComparisonID = this._comparison.ID;
    current.ParticipantID = match.participant.ID;
    current.Term1 = match.match[this.selectedCols[0].name];
    current.Score = match.score;

    this._compService.addMatch(current).subscribe(results => {
      if (this._matches.length) {
        this.saveMatches(this._matches.shift());
      }
    });
  }
}
