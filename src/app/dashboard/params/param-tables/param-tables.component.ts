import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';
import { ParamTablesService } from '../../../shared/services/param-tables.service';
import { ParamTable, TableType } from '../../../shared/models/params.model';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-param-tables',
  templateUrl: 'param-tables.component.html',
  styleUrls: ['./param-tables.component.sass']
})
export class ParamTablesComponent implements OnInit {
  _table: TableOptions = {};
  tables: Observable<ParamTable[]>;
  _tableTypes: TableType[];

  constructor(
    private _tablesService: ParamTablesService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this._table.title = 'Tablas';
    this._table.editable = true;
    this._table.creatable = true;
    this._table.detailsURL = [];
    this._table.addMethod = 'modal';
    this._table.style = ' table-squared';

    this._tableTypes = [
      {
        id: 1,
        name: 'Simple',
        englishName: 'Simple'
      },
      {
        id: 2,
        name: 'Complejo',
        englishName: 'Complex'
      }
    ];

    this._table.columns = [
      {
        name: 'name',
        title: 'Nombre',
        sortable: true,
        filterable: true,
        required: true
      },
      {
        name: 'englishName',
        title: 'Nombre Inglés',
        sortable: true,
        required: true
      },
      {
        name: 'type',
        title: 'Tipo',
        sortable: true,
        type: 'object',
        list: this._tableTypes,
        listID: 'id',
        listDisplay: 'name',
        objectColumn: 'type.name',
        objectID: 'tableTypeId',
        required: true
      },
      {
        name: 'createDate',
        title: 'Fec. Creación',
        sortable: true,
        type: 'datetime',
        readonly: true
      }
    ];
    this._table.pageable = true;

    this.tables = this._tablesService.getTables().map(response => {
      return response.data;
    });
  }

  addTable(table: ParamTable) {
    this._tablesService.createTable(table).subscribe(
      response => {
        this.toastr.success(response.data.name, 'Tabla Creada');
        this.tables = this._tablesService.getTables().map(responsed => {
          return responsed.data;
        });
      },
      (err: Error) => {
        this.toastr.error(err.message, 'Ocurrió un error');
      }
    );
  }

  editTable(table: ParamTable) {
    this._tablesService.editTable(table.id, table).subscribe(
      data => {
        this.toastr.success(table.name, 'Tabla Editada');
      },
      (err: Error) => {
        this.toastr.error(err.message, 'Ocurrió un error');
      }
    );
  }

  deleteTable(id: number) {
    this._tablesService.deleteTable(id).subscribe(
      data => {
        this.toastr.info('Tabla eliminada');
      },
      (err: Error) => {
        this.toastr.error(err.message, 'Ocurrió un error');
      }
    );
  }
}
