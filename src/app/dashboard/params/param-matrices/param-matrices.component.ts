import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';
import { ParamMatrix, MatrixType } from '../../../shared/models/params.model';
import { ParamMatricesService } from '../../../shared/services/param-matrices.service';
import { Observable } from 'rxjs/Observable';

@Component({
  selector: 'app-param-matrices',
  templateUrl: './param-matrices.component.html',
  styleUrls: ['./param-matrices.component.sass']
})
export class ParamMatricesComponent implements OnInit {
  _matrices: Observable<ParamMatrix[]>;
  _matrixTypes: MatrixType[];
  _table: TableOptions = {};

  constructor(
    private _matrixService: ParamMatricesService,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this._table.addMethod = 'modal';
    this._table.creatable = true;
    this._table.editable = true;
    this._table.pageable = true;
    this._table.detailsURL = [];
    this._table.style = ' table-squared';
    this._table.title = 'Matrices';

    this._matrixTypes = [
      {
        id: 1,
        name: 'Individuos',
        englishName: 'Individuals'
      },
      {
        id: 2,
        name: 'Entidades',
        englishName: 'Entities'
      }
    ];

    this._table.columns = [
      { name: 'code', title: 'Código', sortable: true, required: true },
      { name: 'name', title: 'Nombre', sortable: true, required: true },
      {
        name: 'type',
        title: 'Tipo',
        sortable: true,
        type: 'object',
        list: this._matrixTypes,
        listID: 'id',
        listDisplay: 'name',
        objectColumn: 'type.name',
        objectID: 'matrixTypeId',
        required: true
      },
      {
        name: 'description',
        title: 'Descripción',
        type: 'text',
        sortable: true
      },
      {
        name: 'createDate',
        title: 'Fec. Creación',
        type: 'datetime',
        readonly: true
      }
    ];

    this._matrices = this._matrixService.getMatrices().map(response => {return response.data});
  }

  createMatrix(matrix: ParamMatrix) {
    matrix.createDate = new Date();
    this._matrixService.createMatrix(matrix).subscribe(
      response => {
        this.toastr.success(response.data.name, 'Matriz creada');
        this._matrices = this._matrixService.getMatrices().map(responsed => {return responsed.data});
      },
      (err: Error) => {
        this.toastr.error(err.message, err.name);
      }
    );
  }

  updateMatrix(matrix: ParamMatrix) {
    this._matrixService.updateMatrix(matrix.id, matrix).subscribe(
      data => {
        this._matrices = this._matrixService.getMatrices().map(response => {return response.data});
        this.toastr.success(matrix.name, 'Matrix Updated');
      },
      (error: Error) => {
        this.toastr.error(error.message, error.name);
      }
    );
  }
}
