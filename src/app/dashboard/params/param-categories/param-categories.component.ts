import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { ParamTable, ParamCategory } from '../../../shared/models/params.model';
import { ParamCategoriesService } from '../../../shared/services/param-categories.service';
import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';

@Component({
  selector: 'param-categories',
  templateUrl: './param-categories.component.html',
  styleUrls: ['./param-categories.component.sass']
})
export class ParamCategoriesComponent implements OnInit {
  @Input() matrixID: number;

  _table: TableOptions = {};
  _tables: ParamTable[];
  _categories: ParamCategory[];
  _newCategory: ParamCategory;
  _currentCategory: ParamCategory;
  _newCategories: ParamCategory[] = [];

  constructor(
    private _categoriesService: ParamCategoriesService,
    private toast: ToastrService
  ) {}

  ngOnInit() {
    this._table.columns = [
      {
        name: 'name',
        title: 'Nombre',
        sortable: true,
        filterable: true,
        required: true
      },
      {
        name: 'englishName',
        title: 'Nombre Inglés',
        sortable: true,
        filterable: true,
        required: true
      },
      {
        name: 'weighting',
        title: 'Ponderación',
        sortable: true,
        required: true
      }
    ];

    this._table.editable = true;
    this._table.deletable = true;
    this._table.style = ' table-squared';
    this._table.addMethod = 'modal';
    this.getCategories();
  }

  getCategories() {
    this._categoriesService
      .getCategoriesByMatrix(this.matrixID)
      .subscribe(response => {
        this._categories = response.data;
      });
  }

  addCategory(cat: ParamCategory) {
    cat.paramMatrixId = this.matrixID;
    this._categoriesService.createCategory(cat).subscribe(response => {
      this.toast.success(response.data.name, 'Categoría creada exitosamente');
      this._categories.push(response.data);
    });
  }

  editCategory(cat: ParamCategory) {
    this._categoriesService.editCategory(cat.id, cat).subscribe(data => {
      this.toast.success(cat.name, 'Categoría editada exitosamente');
    });
  }
}
