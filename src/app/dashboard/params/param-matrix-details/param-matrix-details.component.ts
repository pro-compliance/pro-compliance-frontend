import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ParamMatrix } from '../../../shared/models/params.model';
import { ParamMatricesService } from '../../../shared/services/param-matrices.service';

@Component({
  moduleId: module.id,
  selector: 'param-matrix-details',
  templateUrl: './param-matrix-details.component.html',
  styleUrls: ['./param-matrix-details.component.sass']
})

export class ParamMatrixDetailsComponent implements OnInit {

  _matrix: ParamMatrix;

  constructor(
    private _route: ActivatedRoute,
    private _matrixService: ParamMatricesService
  ) { }

  ngOnInit() {
    this.initMatrix();
  }

  initMatrix() {
    this._route.params.subscribe(params => {
      this._matrixService.getMatrix(params['id'])
      .subscribe(response => {
        this._matrix = response.data;
      });
    });
  }
}
