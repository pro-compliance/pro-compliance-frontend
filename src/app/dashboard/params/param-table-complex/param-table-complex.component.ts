import { Component, OnInit, Input } from '@angular/core';
import { ToastrService } from 'ngx-toastr';

import { ParamTablesService } from '../../../shared/services/param-tables.service';
import { ParamValuesService } from '../../../shared/services/param-values.service';
import { ParamTable, ParamValue } from '../../../shared/models/params.model';

@Component({
  moduleId: module.id,
  selector: 'param-table-complex',
  templateUrl: './param-table-complex.component.html',
  styleUrls: ['./param-table-complex.component.sass']
})
export class ParamTableComplexComponent implements OnInit {
  @Input() table: ParamTable;

  _newValue: ParamValue = {};
  _currentValue: ParamValue;
  _saving: boolean;
  _editing: boolean;
  _values: ParamValue[];

  constructor(private _tableService: ParamTablesService, private _valueService: ParamValuesService, private toastr: ToastrService) {}

  ngOnInit() {
    this._valueService.getValuesByTable(this.table.id).subscribe(response => {
      this._values = response.data;
    });
  }

  onSubmit() {
    this._newValue.paramTableId = this.table.id;
    this._valueService.addValue(this._newValue).subscribe(response => {
      this.toastr.success(response.data.displayValue, 'Categoría creada');
      response.data.subValues = [];
      this._values.push(response.data);
      this._newValue = {};
    });
  }
}
