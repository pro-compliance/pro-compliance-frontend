import { Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';

import { UserService } from '../../../shared/services/users.service';
import { User, Role } from '../../../shared/models/users.model';
import { TableOptions } from '../../../shared/components/custom-table/custom-table.options';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.sass']
})
export class UsersListComponent implements OnInit {
  _users: Observable<User[]>;
  _table: TableOptions = {};
  _roles: Role[];

  constructor(private _userServ: UserService, private toast: ToastrService) {}

  ngOnInit() {
    this._roles = [
      {
        id: 1,
        name: 'Administrador',
        englishName: 'Administrator',
        admin: true
      },
      { id: 2, name: 'Usuario', englishName: 'User', admin: true }
    ];

    this._table.columns = [
      {
        name: 'id',
        title: '#',
        sortable: true,
        filterable: true,
        readonly: true
      },
      { name: 'userName', title: 'Nombre', sortable: true, filterable: true },
      {
        name: 'role',
        title: 'Rol',
        type: 'object',
        objectColumn: 'role.name',
        objectID: 'roleId',
        list: this._roles,
        listID: 'id',
        listDisplay: 'name'
      },
      { name: 'email', title: 'Email', sortable: true, filterable: true },
      {
        name: 'createDate',
        title: 'Fecha Creación',
        type: 'datetime',
        sortable: true,
        readonly: true
      }
    ];

    this._table.style = ' table-squared';

    this._table.pageable = true;

    this._table.editable = true;

    this._table.addMethod = 'modal';

    this._table.searcheable = true;

    this._table.newURL = ['nuevo'];

    this._table.title = 'Usuarios';

    this._table.showTitle = true;

    this._users = this._userServ.getUsers().map(response => {return response.data});
  }

  updateUser(user: User) {
    this._userServ.updateUser(user.id, user).subscribe(
      response => {
        this.toast.success(response.data.userName, 'Usuario actualizado');
        this._users = this._userServ.getUsers().map(responsed => {return responsed.data});
      },
      (err: Error) => {
        this.toast.error(err.message, 'Ocurrió un error');
      }
    );
  }

  deleteUser(id: number) {
    this._userServ.deleteUser(id).subscribe(
      data => {
        this.toast.info('Usuario eliminado');
      },
      (err: Error) => {
        this.toast.error(err.message, 'Ocurrió un error');
      }
    );
  }
}
