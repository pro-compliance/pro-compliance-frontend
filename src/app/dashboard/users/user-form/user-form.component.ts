import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Validators, FormControl, FormGroup } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';

import { User, Role } from '../../../shared/models/users.model';
import { UserService } from '../../../shared/services/users.service';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.sass']
})
export class UserFormComponent implements OnInit {
  _userForm: FormGroup;
  _user: User = {};
  _roles: Role[];

  constructor(
    private _userServ: UserService,
    private _router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    this._roles = [
      {
        id: 1,
        name: 'Administrador',
        englishName: 'Administrator',
        admin: true
      },
      { id: 2, name: 'Usuario', englishName: 'User', admin: false },
      { id: 3, name: 'Demo', englishName: 'Demo', admin: false }
    ];
    this._userForm = new FormGroup(
      {
        userName: new FormControl('', [
          Validators.required,
          Validators.minLength(5)
        ]),
        email: new FormControl('', Validators.required),
        roleId: new FormControl('', Validators.required),
        password: new FormControl('', [
          Validators.required,
          Validators.minLength(6)
        ]),
        passwordConfirm: new FormControl('', [
          Validators.required,
          Validators.minLength(6)
        ])
      },
      this.passwordConfirm
    );
  }

  saveUser(model: User) {
    model.createDate = new Date();
    model.accountId = 1;
    model.active = true;
    this._userServ.createUser(model).subscribe(
      response => {
        this.toastr.success('Usuario creado exitosamente', model.userName);
        this._router.navigate(['app/usuarios']);
      },
      (err: HttpErrorResponse) => {
        this.toastr.error('Ocurrió un error', err.message);
      }
    );
  }

  passwordConfirm(g: FormGroup) {
    return g.get('password').value === g.get('passwordConfirm').value
      ? null
      : { mismatch: true };
  }
}
