import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

import { AuthService } from '../../shared/services/auth.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  _login: any = {
    userName: '',
    password: ''
  };

  _invalid = false;
  _loading = false;

  constructor(
    private _authServ: AuthService,
    private _router: Router,
    private toastr: ToastrService
  ) {}

  ngOnInit() {
    if (this._authServ.isLogged()) {
      this.toastr.info('Sesión finalizada', 'Adiós');
      this._authServ.authLogout().subscribe(data => {
        console.log(data);
      });
    }
  }

  signIn() {
    this._loading = true;
    this._authServ.authLogin(this._login).subscribe(
      response => {
        if (response) {
          this._authServ.setCurrentSession(response.data);
          const _user = this._authServ.getUserInfo();
          this.toastr.success(_user.userName, 'Bienvenido');
          this._loading = false;
          this._router.navigate(['/']);
        } else {
          this._loading = false;
          this._invalid = true;
        }
      },
      (err: HttpErrorResponse) => {
        this.toastr.error(err.error.message);
        this._loading = false;
        this._invalid = true;
      }
    );
  }
}
