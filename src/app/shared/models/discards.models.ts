import { SanctionList } from './sanctions.model';
import { Participant } from './participants.model';
export interface ParticipantDiscard {
  id?: number;
  participantId?: number;
  participant?: Participant;
  sanctionListId?: number;
  sanctionList?: SanctionList;
  date?: Date;
  match?: boolean;
}
