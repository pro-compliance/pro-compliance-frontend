export interface Pending {
  id?: number;
  pendingTypeId?: number;
  title?: string;
  description?: string;
  createDate?: Date;
  beginDate?: Date;
  expirationDate?: Date;
  completedDate?: Date;
  participantId?: number;
  stageId?: number;
}

export interface Stage {
  id?: number;
  name?: string;
  englishName?: string;
}
