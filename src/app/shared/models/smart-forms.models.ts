export interface SmartForm {
  id?: number;
  title?: string;
  fields?: FormField[];
  description?: string;
  createUserId: number;
  createDate?: Date;
  modificationDate?: Date;
}

export interface FieldType {
  id?: number;
  name?: string;
  englishName?: string;
  multipleOptions?: boolean
}

export interface FormField {
  id?: number;
  formId?: number;
  typeId?: number;
  type?: FieldType;
  name?: string;
  description?: string;
  required?: boolean;
}
