import { Participant } from './participants.model';

export interface Alert {
  id?: number;
  alertSourceId?: number;
  alertSource?: AlertSource;
  alertReasonId?: number;
  alertReason?: AlertReason;
  description?: string;
  createDate?: Date;
  participantId?: number;
  cleared?: boolean;
  clearedDate?: Date;
  clarification?: string;
  clearedBy?: number;
}

export interface AlertSource {
  id?: number;
  name?: string;
  englishName?: string;
}

export interface AlertReason {
  id?: number;
  alertSourceId?: number;
  source?: AlertSource;
  name?: string;
  englishName?: string;
  alertPriorityId?: number;
  priority?: AlertPriority;
  code?: string;
}

export interface ParticipantAlert {
  ID?: number;
  ParticipantID?: number;
  Participant?: Participant;
  AlertTypeID?: number;
  AlertType?: AlertType;
  Name?: string;
  Description?: string;
  Discard?: Boolean;
  Date?: Date;
  Clarification?: string;
  DiscardedUser?: number;
}

export interface AlertType {
  ID?: number;
  Name?: string;
}

export interface AlertPriority {
  id?: number;
  name?: string;
  englishName?: string;
}
