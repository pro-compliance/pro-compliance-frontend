export interface User {
  id?: number;
  roleId?: number;
  role?: Role;
  accountId?: number;
  userName?: string;
  password?: string;
  email?: string;
  active?: boolean;
  createDate?: Date;
  lastChangePassword?: Date;
}

export interface Role {
  id?: number;
  name?: string;
  englishName?: string;
  admin?: boolean;
}

export interface Session {
  id?: number;
  userId?: number;
  sessionId?: string;
  loginTime?: Date;
  logoutTime?: Date;
  ip?: string;
  user?: User;
}
