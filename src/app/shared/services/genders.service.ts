import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';

@Injectable()
export class GendersService {
  private _gendersURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._gendersURL = _conn.APIUrl + 'genders';
  }

  getGenders() {
    return this._http.get(this._gendersURL);
  }
}
