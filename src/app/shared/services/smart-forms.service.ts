import { Injectable } from '@angular/core';
import { CustomHttpClient } from './custom-http.service';
import { ConnectionService } from './connection.service';

@Injectable({ providedIn: 'root' })
export class SmartFormsService {
  private _formsUrl: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._formsUrl = this._conn.APIUrl + 'smartforms';
  }

  getForms() {
    return this._http.get(this._formsUrl);
  }

  getForm(id: number) {
    return this._http.get(`${this._formsUrl}/${id}`);
  }
}
