import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { SanctionList } from '../models/sanctions.model';

@Injectable()
export class SanctionListsService {
  _listURl: string;
  _matchesURL: string;

  constructor(private _http: CustomHttpClient, private _conn: ConnectionService) {
    this._listURl = _conn.APIUrl + 'sanctionlists';
    this._matchesURL = _conn.APIUrl + 'sanctionmatches';
  }

  runDiscard() {
    return this._http.get(this._matchesURL + '/run/all');
  }

  getLists() {
    return this._http.get(this._listURl);
  }

  getItemsByList(id: number) {
    return this._http.get(`${this._listURl}/${id}/items`);
  }

  getList(id: number) {
    return this._http.get(`${this._listURl}/${id}`);
  }

  loadList(list: SanctionList) {
    return this._http.post(`${this._listURl}/load`, list);
  }

  addList(list: SanctionList) {
    return this._http.post(this._listURl, JSON.stringify(list));
  }

  updateList(id: number, list: SanctionList) {
    return this._http.put(this._listURl, id, list);
  }
}
