import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { ParticipantProfile, ProfileAccount } from '../models/profiles.model';

@Injectable()
export class ParticipantProfilesService {
  private _profilesURL: string;
  private _accountsURL: string;
  private _dashboardURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._accountsURL = _conn.APIUrl + 'profileaccounts';
    this._profilesURL = _conn.APIUrl + 'participantprofiles';
    this._dashboardURL = _conn.APIUrl + 'financialdashboards';
  }

  getProfile(id: number) {
    return this._http.get(`${this._profilesURL}/${id}`);
  }

  updateProfile(id: number, profile: ParticipantProfile) {
    return this._http.put(this._profilesURL, id, profile);
  }

  createAccount(account: ProfileAccount) {
    return this._http.post(this._accountsURL, account);
  }

  editAccount(id: number, account: ProfileAccount) {
    return this._http.put(this._accountsURL, id, account);
  }

  getDashboards() {
    return this._http.get(this._dashboardURL);
  }

  getDashboard(id: number) {
    return this._http.get(`${this._dashboardURL}/${id}`);
  }
}
