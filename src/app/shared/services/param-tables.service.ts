import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';

import { ConnectionService } from './connection.service';
import { ParamTable, ParamSubValue } from './../models/params.model';

@Injectable()
export class ParamTablesService {
  private _tablesURL: string;
  private _subValuesURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._tablesURL = _conn.APIUrl + 'paramtables';
    this._subValuesURL = _conn.APIUrl + 'paramsubvalues';
  }

  getTables() {
    return this._http.get(this._tablesURL);
  }

  getTable(_id: number) {
    return this._http.get(`${this._tablesURL}/${_id}`);
  }

  createTable(tab: ParamTable) {
    return this._http.post(this._tablesURL, tab);
  }

  editTable(id: number, table: ParamTable) {
    return this._http.put(this._tablesURL, id, table);
  }

  deleteTable(id: number) {
    return this._http.delete(`${this._tablesURL}/${id}`);
  }

  addSubValue(val: ParamSubValue) {
    return this._http.post(this._subValuesURL, val);
  }

  editSubValue(id: number, val: ParamSubValue) {
    return this._http.put(this._subValuesURL, id, val);
  }
}
