import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Task } from '../models/tasks.model';

@Injectable()
export class TasksService {
  private _taskURL: string;
  private _taskStatusURL: string;
  private _eventsURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._taskURL = _conn.APIUrl + 'tasks';
    this._taskStatusURL = _conn.APIUrl + 'taskstatus';
    this._eventsURL = _conn.APIUrl + 'tasksevents';
  }

  getTasks() {
    return this._http.get(this._taskURL);
  }

  getTask(id: number) {
    return this._http.get(`${this._taskURL}/${id}`);
  }

  getTasksByCategory(id: number) {
    return this._http.get(`${this._taskURL}/category/${id}`);
  }

  getEvents() {
    return this._http.get(this._eventsURL);
  }

  getTaskCount(id: number) {
    return this._http.get(`${this._taskURL}/category/${id}/count`);
  }

  getTaksByParticipant() {
    return this._http.get(`${this._taskURL}/byparticipant`);
  }

  createTasks(task: Task) {
    return this._http.post(this._taskURL, task);
  }

  getStatus() {
    return this._http.get(this._taskStatusURL);
  }

  updateTask(_id: number, _task: Task) {
    return this._http.put(this._taskURL, _id, _task);
  }
}
