import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';

import { ConnectionService } from './connection.service';
import { ParamValue } from './../models/params.model';

@Injectable()
export class ParamValuesService {
  private _tablesURL: string;
  private _valuesURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._tablesURL = _conn.APIUrl + 'paramtables';
    this._valuesURL = _conn.APIUrl + 'paramvalues';
  }

  getValuesByTable(id: number) {
    return this._http.get(`${this._tablesURL}/${id}/values`);
  }

  addValue(val: ParamValue) {
    return this._http.post(this._valuesURL, val);
  }

  editValue(id: number, val: ParamValue) {
    return this._http.put(this._valuesURL, id, val);
  }

  deleteValue(id: number) {
    return this._http.delete(`${this._valuesURL}/${id}`);
  }
}
