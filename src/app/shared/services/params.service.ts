import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Param } from '../models/params.model';

@Injectable()
export class ParamsService {
  private _paramsURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._paramsURL = _conn.APIUrl + 'params';
  }

  getParams() {
    return this._http.get(this._paramsURL);
  }

  getParam(id: number) {
    return this._http.get(`${this._paramsURL}/${id}`);
  }

  addParams(par: Param) {
    return this._http.post(this._paramsURL, par);
  }

  editParam(id: number, param: Param) {
    return this._http.put(this._paramsURL, id, param);
  }

  deleteParam(id: number) {
    return this._http.delete(`${this._paramsURL}/${id}`);
  }
}
