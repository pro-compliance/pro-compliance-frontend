import { HttpResponse } from './../models/response.model';
import { AuthService } from './auth.service';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CustomHttpClient {
  constructor(private _http: HttpClient, private _auth: AuthService) {}

  createSessionHeader(file?: boolean): HttpHeaders {
    const header = new HttpHeaders({
      userId: this._auth.getUserInfo().id.toString(),
      sessionId: this._auth.getCurrentSession().sessionId.toString()
    });
    if (!file) {
      header.append('Content-Type', 'application/json');
    }
    return header;
  }

  get(url: string) {
    const _headers = this.createSessionHeader();
    return this._http.get<HttpResponse>(url, { headers: _headers });
  }

  post(url: string, data: any, file?: boolean) {
    const _headers = this.createSessionHeader(file);
    return this._http.post<HttpResponse>(url, data, {
      headers: _headers
    });
  }

  put(url: string, id: number, data: any) {
    const _headers = this.createSessionHeader();
    return this._http.put<HttpResponse>(`${url}/${id}`, data, {
      headers: _headers
    });
  }

  delete(url: string) {
    const _headers = this.createSessionHeader();
    return this._http.delete(url, { headers: _headers });
  }
}
