import { Injectable } from '@angular/core';
import { CustomHttpClient } from './custom-http.service';
import { ConnectionService } from './connection.service';
import { FormField } from '../models/smart-forms.models';

@Injectable({ providedIn: 'root' })
export class FormFieldsService {
  _fieldsURL: string;
  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._fieldsURL = _conn.APIUrl + 'formfields';
  }

  createField(field: FormField) {
    return this._http.post(this._fieldsURL, field);
  }

  updateField(id: number, field: FormField) {
    return this._http.put(this._fieldsURL, id, field);
  }

  deleteField(id: number) {
    return this._http.delete(`${this._fieldsURL}/${id}`);
  }
}
