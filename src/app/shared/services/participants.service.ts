import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';

import { Participant, ParticipantParam } from '../models/participants.model';
import { ConnectionService } from './connection.service';
import { UtilService } from './util.service';
import { AuthService } from './auth.service';

@Injectable()
export class ParticipantsService {
  private _partURL: string;
  private _paramURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService,
    private _auth: AuthService,
    private _util: UtilService
  ) {
    this._partURL = _conn.APIUrl + 'participants';
    this._paramURL = _conn.APIUrl + 'participantparams';
  }

  getParticipants(type?: string) {
    if (type) {
      return this._http.get(`${this._partURL}/${type}`);
    } else {
      return this._http.get(this._partURL);
    }
  }

  getSegments(paramId: number) {
    return this._http.get(`${this._partURL}/segments/${paramId}`);
  }

  getCount() {
    return this._http.get(`${this._partURL}/count`);
  }

  getSegmentMembers(paramId: number, valueId: number) {
    return this._http.get(
      `${this._partURL}/byparam/${paramId}/value/${valueId}`
    );
  }

  getParticipant(_id: number) {
    return this._http.get(`${this._partURL}/${_id}`);
  }

  getLastParticipants() {
    return this._http.get(`${this._partURL}/last`);
  }

  createParticipant(part: Participant) {
    const _user = this._auth.getUserInfo(); // get Current User
    part.createdUserId = _user.id; // set User ID
    return this._http.post(this._partURL, part);
  }

  updateParticipant(_id: number, _part: Participant) {
    return this._http.put(this._partURL, _id, _part);
  }

  searchParticipant(
    participants: Participant[],
    search: string
  ): Participant[] {
    const filterParticipants: Participant[] = [];

    search = search.toLocaleLowerCase();

    for (const item of participants) {
      let term =
        this._util.isNullString(item.firstName) +
        this._util.isNullString(item.secondName);
      term =
        term +
        this._util.isNullString(item.thirdName) +
        this._util.isNullString(item.fourthName);
      term = term.toLocaleLowerCase();

      if (term.indexOf(search) >= 0) {
        filterParticipants.push(item);
      }
    }

    return filterParticipants;
  }

  getParams(_partID: number) {
    return this._http.get(`${this._partURL}/${_partID}/params`);
  }

  getParticipantParam(participantID: number, paramID: number) {
    return this._http.get(
      `${this._partURL}/${participantID}/params/${paramID}`
    );
  }

  updateParam(_id: number, _param: ParticipantParam) {
    return this._http.put(this._paramURL, _id, _param);
  }

  getRate(_part: Participant): string {
    if (!_part.score) {
      return 'Incompleto';
    } else if (_part.score < 3) {
      return 'Bajo';
    } else if (_part.score < 6) {
      return 'Medio';
    } else {
      return 'Alto';
    }
  }

  getParticipantsbyRisk() {
    return this._http.get(`${this._partURL}/byrisk`);
  }

  getParticipantsbyCountry() {
    return this._http.get(`${this._partURL}/bycountry`);
  }

  getPendingDocuments(id: number) {
    return this._http.get(`${this._partURL}/${id}/pending`);
  }

  getDiligence(id: number) {
    return this._http.get(`${this._partURL}/${id}/diligence`);
  }
}
