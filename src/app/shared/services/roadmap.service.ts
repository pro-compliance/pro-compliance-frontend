import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Roadmap, Phase, Milestone } from '../models/roadmap.model';

@Injectable()
export class RoadmapService {
  private _roadmapURL: string;
  private _phaseURL: string;
  private _milesURL: string;
  private _recurrenceURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService,
  ) {
    this._roadmapURL = _conn.APIUrl + 'roadmaps';
    this._phaseURL = _conn.APIUrl + 'phases';
    this._milesURL = _conn.APIUrl + 'milestones';
    this._recurrenceURL = _conn.APIUrl + 'recurrences';
  }

  getRoadmaps() {
    return this._http.get(this._roadmapURL);
  }

  updateRoadmap(id: number, roadmap: Roadmap) {
    return this._http.put(this._roadmapURL, id, roadmap);
  }

  getRoadmap(id: number) {
    return this._http.get(`${this._roadmapURL}/${id}`);
  }

  createRoadmap(roadmap: Roadmap) {
    return this._http.post(this._roadmapURL, roadmap);
  }

  createPhase(phase: Phase) {
    return this._http.post(this._phaseURL, phase);
  }

  updatePhase(id: number, phase: Phase) {
    return this._http.put(this._phaseURL, id, phase);
  }

  createMilestone(milestone: Milestone) {
    return this._http.post(this._milesURL, milestone);
  }

  getRecurrence() {
    return this._http.get(this._recurrenceURL);
  }
}
