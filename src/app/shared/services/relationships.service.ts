import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';

import { ConnectionService } from './connection.service';
import {
  RelationshipType,
  ParticipantRelationship
} from '../models/relationships.model';

@Injectable()
export class RelationshipsService {
  private _typesURL: string;
  private _participantsURL: string;
  private _relationshipsURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._typesURL = _conn.APIUrl + 'relationshiptypes';
    this._relationshipsURL = _conn.APIUrl + 'participantrelationships';
    this._participantsURL = _conn.APIUrl + 'participants';
  }

  getRelationships(participantID: number) {
    return this._http.get(
      `${this._participantsURL}/${participantID}/relationships`
    );
  }

  getTypes() {
    return this._http.get(this._typesURL);
  }

  createType(type: RelationshipType) {
    return this._http.post(this._typesURL, type);
  }

  updateType(id: number, type: RelationshipType) {
    return this._http.put(this._typesURL, id, type);
  }

  deleteType(id: number) {
    return this._http.delete(`${this._typesURL}/${id}`);
  }

  addRelationship(relationship: ParticipantRelationship) {
    return this._http.post(this._relationshipsURL, relationship);
  }

  deleteRelationship(id: number) {
    return this._http.delete(`${this._relationshipsURL}/${id}`);
  }
}
