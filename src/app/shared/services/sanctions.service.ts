import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Sanction, Discard, DiscardMatch } from '../models/sanctions.model';
import { ParticipantsService } from './participants.service';
import { UtilService } from './util.service';
import { Participant } from '../models/participants.model';

@Injectable()
export class SanctionsService {
  private _listsURL: string;
  private _discardURL: string;
  private _matchesURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService,
    private _partService: ParticipantsService,
    private _util: UtilService
  ) {
    this._listsURL = _conn.APIUrl + 'lists';
    this._discardURL = _conn.APIUrl + 'discards';
    this._matchesURL = _conn.APIUrl + 'discardmatches';
  }

  getLists() {
    return this._http.get(this._listsURL);
  }

  getSanctionsByList(listID: number) {
    return this._http.get(`${this._listsURL}/${listID}/sanctions`);
  }

  getDiscards() {
    return this._http.get(this._discardURL);
  }

  addDiscard(listID: number) {
    const _discard: Discard = {};
    _discard.ListID = listID;
    return this._http.post(this._discardURL, _discard);
  }

  runDiscard(discardID: number, sanctions: Array<Sanction>): Promise<Array<any>> {
    const concurrences: Array<any> = [];

    return new Promise(resolve => {
      this._partService.getParticipants().subscribe(response => {
        const participants = <Participant[]>response.data;
        sanctions.forEach(sanction => {
          const terms = sanction.Term1.toLocaleLowerCase().split(' ');
          participants.forEach(participant => {
            let searchTerm = this._util.isNullString(participant.firstName);
            searchTerm = searchTerm + this._util.isNullString(participant.thirdName);
            searchTerm = searchTerm.toLocaleLowerCase();
            terms.forEach(term => {
              if (searchTerm.indexOf(term) >= 0 && term.length > 4) {
                const match = {
                  sanctionID: sanction.ID,
                  participantID: participant.id
                };
                concurrences.push(match);
              }
            });
          });
        });
        resolve(concurrences);
      });
    });
  }

  getMatches(discardID: number) {
    return this._http.get(`${this._discardURL}/${discardID}/matches`);
  }

  saveMatches(discardID: number, sanctionID: number, participantID: number) {
    const _match: DiscardMatch = {};
    _match.DiscardID = discardID;
    _match.SanctionID = sanctionID;
    _match.ParticipantID = participantID;
    return this._http.post(this._matchesURL, _match);
  }

  validMatch(matchID: number, valid: boolean) {
    let _valid: string;
    if (valid === true) {
      _valid = 'valid';
    } else {
      _valid = 'invalid';
    }
    return this._http.get(`${this._discardURL}/matches/${matchID}/${_valid}`);
  }
}
