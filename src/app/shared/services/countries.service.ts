import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';

import { Country } from '../models/country.model';
import { ConnectionService } from './connection.service';

@Injectable()
export class CountriesService {
  private _countryURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._countryURL = _conn.APIUrl + 'countries';
  }

  getCountries() {
    return this._http.get(this._countryURL);
  }

  addCountry(country: Country) {
    return this._http.post(this._countryURL, country);
  }

  editCountry(id: number, country: Country) {
    return this._http.put(this._countryURL, id, country);
  }

  deleteCountry(id: number) {
    return this._http.delete(`${this._countryURL}/${id}`);
  }
}
