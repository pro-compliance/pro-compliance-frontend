import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { FinancialProduct, ProfileProduct } from '../models/products.model';

@Injectable()
export class FinancialProductsService {
  private _productsURL: string;
  private _profileProductURL: string;
  private _profileURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._productsURL = _conn.APIUrl + 'financialproducts';
    this._profileProductURL = _conn.APIUrl + 'profileproducts';
    this._profileURL = _conn.APIUrl + 'participantprofiles';
  }

  getProducts() {
    return this._http.get(this._productsURL);
  }

  createProduct(product: FinancialProduct) {
    return this._http.post(this._productsURL, product);
  }

  editProduct(id: number, product: FinancialProduct) {
    return this._http.put(this._productsURL, id, product);
  }

  deleteProduct(id: number) {
    return this._http.delete(`${this._productsURL}/${id}`);
  }

  addProfileProduct(product: ProfileProduct) {
    return this._http.post(this._profileProductURL, product);
  }

  getProfileProducts(id: number) {
    return this._http.get(
      `${this._profileURL}/${id}/products`
    );
  }

  editProfileProduct(id: number, product: ProfileProduct) {
    return this._http.put(this._profileProductURL, id, product);
  }

  deleteProfileProduct(id: number) {
    return this._http.delete(`${this._profileProductURL}/${id}`);
  }
}
