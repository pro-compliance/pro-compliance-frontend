import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { ParamCategory } from '../models/params.model';

@Injectable()
export class ParamCategoriesService {
  private _categoryURL: string;
  private _matrxixURL: string;

  constructor(private _http: CustomHttpClient, private _conn: ConnectionService) {
    this._categoryURL = _conn.APIUrl + 'paramcategories';
    this._matrxixURL = _conn.APIUrl + 'parammatrices';
  }

  getCategories() {
    return this._http.get(this._categoryURL);
  }

  getCategoriesByMatrix(matrixID: number) {
    return this._http.get(`${this._matrxixURL}/${matrixID}/categories`);
  }

  createCategory(cat: ParamCategory) {
    return this._http.post(this._categoryURL, cat);
  }

  editCategory(id: number, cat: ParamCategory) {
    return this._http.put(this._categoryURL, id, cat);
  }
}
