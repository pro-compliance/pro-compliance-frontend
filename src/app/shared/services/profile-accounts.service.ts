import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { AccountType } from '../models/profiles.model';

@Injectable()
export class ProfileAccountsService {
  _typesURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._typesURL = _conn.APIUrl + 'accounttypes';
  }

  getTypes() {
    return this._http.get(this._typesURL);
  }

  addType(type: AccountType) {
    return this._http.post(this._typesURL, type);
  }

  editType(id: number, type: AccountType) {
    return this._http.put(this._typesURL, id, type);
  }

  deleteType(id: number) {
    return this._http.delete(`${this._typesURL}/${id}`);
  }
}
