import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';

@Injectable()
export class MatrixTypesService {
  private _matrixTypeURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._matrixTypeURL = _conn.APIUrl + 'MatrixTypes';
  }

  getMatrixTypes() {
    return this._http.get(this._matrixTypeURL);
  }
}
