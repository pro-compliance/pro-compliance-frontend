import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { User } from '../models/users.model';
import { ConnectionService } from './connection.service';

@Injectable()
export class UserService {
  private _usrURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._usrURL = _conn.APIUrl + 'users';
  }

  getUsers() {
    return this._http.get(this._usrURL);
  }

  getUser(_id: number) {
    return this._http.get(`${this._usrURL}/${_id}`);
  }

  updateUser(id: number, user: User) {
    return this._http.put(this._usrURL, id, user);
  }

  deleteUser(id: number) {
    return this._http.delete(`${this._usrURL}/${id}`);
  }

  createUser(usr: User) {
    return this._http.post(this._usrURL, JSON.stringify(usr));
  }
}
