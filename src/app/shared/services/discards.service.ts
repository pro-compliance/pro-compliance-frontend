import { CustomHttpClient } from './custom-http.service';
import { ConnectionService } from './connection.service';
import { Injectable } from '@angular/core';

@Injectable()
export class DiscardsService {
  private _discardsURL: string;
  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._discardsURL = _conn.APIUrl + 'discards';
  }

  uploadExcel(file: File) {
    const formData: FormData = new FormData();
    formData.append('file', file, file.name);
    return this._http.post(`${this._discardsURL}/file`, formData, true);
  }

  getParticipantDiscards(participantId: number) {
    return this._http.get(`${this._discardsURL}/participant/${participantId}`);
  }

  runParticipantDiscards(participantId: number) {
    return this._http.post(
      `${this._discardsURL}/participant/${participantId}`,
      {}
    );
  }
}
