import { Injectable } from '@angular/core';
import { CustomHttpClient } from './custom-http.service';
import { ConnectionService } from './connection.service';
import { FieldType } from '../models/smart-forms.models';

@Injectable({ providedIn: 'root' })
export class FieldTypesService {
  private _typesURL: string;
  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._typesURL = _conn.APIUrl + 'fieldtypes';
  }

  getTypes() {
    return this._http.get(this._typesURL);
  }

  createType(type: FieldType) {
    return this._http.post(this._typesURL, type);
  }
}
