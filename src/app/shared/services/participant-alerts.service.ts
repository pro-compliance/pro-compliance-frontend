import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { ParticipantAlert } from '../models/alerts.model';
import { LocalStorage } from '@ngx-pwa/local-storage';

@Injectable()
export class ParticipantAlertsService {
  _alertsURL: string;
  _participantsURL: string;
  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService,
    protected localStorage: LocalStorage
  ) {
    this._alertsURL = _conn.APIUrl + 'participantalerts';
    this._participantsURL = _conn.APIUrl + 'participants';
  }

  getAlerts() {
    return this._http.get(this._alertsURL);
  }

  getLastAlerts(id: number) {
    return this._http.get(`${this._alertsURL}/after/${id}`);
  }

  getAlertsByParticipant(id: number) {
    return this._http.get(
      `${this._participantsURL}/${id}/alerts`
    );
  }

  createAlert(alert: ParticipantAlert) {
    return this._http.post(this._alertsURL, alert);
  }

  updateAlert(id: number, alert: ParticipantAlert) {
    return this._http.put(this._alertsURL, id, alert);
  }

  initAlerts() {
    this.getAlerts().subscribe(data => {
      this.localStorage.setItem('alerts', data).subscribe(() => {});
    });
  }

  // updateLastAlerts() {
  //   let currentAlerts: ParticipantAlert[];
  //   let lastID: number;
  //   this.localStorage
  //     .getItem('alerts')
  //     .subscribe(alerts  => {
  //       currentAlerts = alerts;
  //       lastID = currentAlerts[currentAlerts.length - 1].ID;
  //       this.getLastAlerts(lastID).subscribe(data => {
  //         data.forEach(element => {
  //           currentAlerts.push(element);
  //         });
  //         this.localStorage
  //           .setItem('alerts', currentAlerts)
  //           .subscribe(() => {});
  //       });
  //     });
  // }
}
