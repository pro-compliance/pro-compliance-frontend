import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Assignment } from '../models/assignment.model';

@Injectable()
export class AssignmentsService {
  private _assignmentsURL: string;
  private _progressURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._assignmentsURL = _conn.APIUrl + 'assignments';
    this._progressURL = _conn.APIUrl + 'progress';
  }

  getProgress() {
    return this._http.get(this._progressURL);
  }

  getAssignments() {
    return this._http.get(this._assignmentsURL);
  }

  getAssignment(id: number) {
    return this._http.get(`${this._assignmentsURL}/${id}`);
  }

  createAssignment(assigment: Assignment) {
    return this._http.post(this._assignmentsURL, assigment);
  }

  updateAssigment(id: number, assignment: Assignment) {
    return this._http.put(this._assignmentsURL, id, assignment);
  }

  deleteAssignment(id: number) {
    return this._http.delete(`${this._assignmentsURL}/${id}`);
  }
}
