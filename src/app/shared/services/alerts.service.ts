import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { AlertReason, Alert } from '../models/alerts.model';

@Injectable()
export class AlertsService {
  private _alertsURL: string;
  private _alertReasonsURL: string;
  private _alertSourcesURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._alertsURL = _conn.APIUrl + 'alerts';
    this._alertSourcesURL = _conn.APIUrl + 'alertssources';
    this._alertReasonsURL = _conn.APIUrl + 'alertsreasons';
  }

  getAllAlerts() {
    return this._http.get(this._alertsURL);
  }

  getAlertsByParticipant(id: number) {
    return this._http.get(`${this._alertsURL}/participant/${id}`);
  }

  createAlert(alert: Alert) {
    alert.createDate = new Date();
    return this._http.post(this._alertsURL, alert);
  }

  clearAlert(id: number, alert: Alert) {
    alert.cleared = true;
    alert.clearedDate = new Date();
    return this._http.put(this._alertsURL, id, alert);
  }

  getSources() {
    return this._http.get(this._alertSourcesURL);
  }

  getSource(id: number) {
    return this._http.get(`${this._alertSourcesURL}/${id}`);
  }

  getReasons() {
    return this._http.get(this._alertReasonsURL);
  }

  getReason(code: string) {
    return this._http.get(
      `${this._alertReasonsURL}/bycode/${code}`
    );
  }

  createReason(reason: AlertReason) {
    return this._http.post(this._alertReasonsURL, reason);
  }

  editReason(id: number, reason: AlertReason) {
    return this._http.put(this._alertReasonsURL, id, reason);
  }

  // processAlert(reason: AlertReason, profile: ParticipantProfile, )
}
