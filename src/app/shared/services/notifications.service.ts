import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';

@Injectable()
export class NotificationService {
  _notificationsURL: string;
  constructor(private _http: CustomHttpClient, private _conn: ConnectionService) {
    this._notificationsURL = _conn.APIUrl + 'notifications';
  }

  getNotification(last: number) {
    return this._http.get(`${this._notificationsURL}/last/${last}`);
  }
}
