import { HttpResponse } from './../models/response.model';
import { Session } from './../models/users.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

import { User } from '../models/users.model';
import { ConnectionService } from './connection.service';

@Injectable()
export class AuthService {
  private _loginURL: string;
  public token: any;
  public _loggedUser: User;
  private _headers = new HttpHeaders({ 'Content-Type': 'application/json' });

  constructor(private _http: HttpClient, private _conn: ConnectionService) {
    this._loginURL = _conn.APIUrl + 'auth';
  }

  authLogin(login: any): Observable<HttpResponse> {
    return this._http.post(`${this._loginURL}/login`, JSON.stringify(login), {
      headers: this._headers
    });
  }

  setCurrentSession(session: Session) {
    localStorage.setItem('currentUser', JSON.stringify(session.user));
    localStorage.setItem('currentSession', JSON.stringify(session));
  }

  setCurrentUser(token: User) {
    localStorage.setItem(
      'currentUser',
      JSON.stringify({ username: token.userName, token: token })
    );
  }

  getUserInfo(): User {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  clearSession() {
    localStorage.removeItem('currentUser');
    localStorage.removeItem('currentSession');
  }

  getCurrentSession(): Session {
    return JSON.parse(localStorage.getItem('currentSession'));
  }

  isLogged(): boolean {
    const session = this.getCurrentSession();
    console.log(session);
    if (session) {
      return true;
    } else {
      return false;
    }
  }

  authLogout(): Observable<any> {
    const _sessionId = JSON.stringify(this.getCurrentSession().sessionId);
    this.clearSession();
    return this._http.post(`${this._loginURL}/logout`, _sessionId, {
      headers: this._headers
    });
  }
}
