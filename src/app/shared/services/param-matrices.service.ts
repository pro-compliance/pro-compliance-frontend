import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';

import { ConnectionService } from './connection.service';
import { ParamMatrix } from '../models/params.model';

@Injectable()
export class ParamMatricesService {
  private _matrixURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._matrixURL = _conn.APIUrl + 'parammatrices';
  }

  getMatrices() {
    return this._http.get(this._matrixURL);
  }

  getMatrix(_id: number) {
    return this._http.get(`${this._matrixURL}/${_id}`);
  }

  createMatrix(mat: ParamMatrix) {
    console.log(mat);
    return this._http.post(this._matrixURL, mat);
  }

  updateMatrix(id: number, matrix: ParamMatrix) {
    return this._http.put(this._matrixURL, id, matrix);
  }
}
