import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';

@Injectable()
export class FilesService {
  private filesURL: string;
  constructor(private _http: CustomHttpClient, private _conn: ConnectionService) {
    this.filesURL = _conn.APIUrl + 'uploadfiles';
  }

  uploadFiles(files: File[]) {

    if (files.length > 0) {
      const formData: FormData = new FormData();

      for (let i = 0; i < files.length; i++) {
        formData.append('file', files[i], files[i].name);
      }
      return this._http.post(this.filesURL, formData, true);
    }
  }
}
