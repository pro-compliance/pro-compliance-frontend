import { CustomHttpClient } from './custom-http.service';
import { Pending } from './../models/pending.model';
import { ConnectionService } from './connection.service';
import { Injectable } from '@angular/core';

@Injectable()
export class PendingService {
  private _pendingsURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._pendingsURL = _conn.APIUrl + 'pendings';
  }

  getPendings() {
    return this._http.get(this._pendingsURL);
  }

  getPending(id: number) {
    return this._http.get(`${this._pendingsURL}/${id}`);
  }

  addPending(pending: Pending) {
    return this._http.post(this._pendingsURL, pending);
  }
}
