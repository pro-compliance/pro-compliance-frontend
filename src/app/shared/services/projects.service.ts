import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Project } from '../models/assignment.model';

@Injectable()
export class ProjectsService {
  _projectsURL: string;
  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._projectsURL = _conn.APIUrl + 'projects';
  }

  getProjects() {
    return this._http.get(this._projectsURL);
  }

  getProject(id: number) {
    return this._http.get(`${this._projectsURL}/${id}`);
  }

  createProject(project: Project) {
    return this._http.post(this._projectsURL, project);
  }

  updateProject(id: number, project: Project) {
    return this._http.put(this._projectsURL, id, project);
  }

  deleteProject(id: number) {
    return this._http.delete(`${this._projectsURL}/${id}`);
  }
}
