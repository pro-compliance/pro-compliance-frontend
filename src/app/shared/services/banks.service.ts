import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { Bank } from '../models/profiles.model';

@Injectable()
export class BanksService {
  private _banksURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._banksURL = _conn.APIUrl + 'banks';
  }

  getBanks() {
    return this._http.get(this._banksURL);
  }

  getBank(id: number) {
    return this._http.get(`${this._banksURL}/${id}`);
  }

  createBank(bank: Bank) {
    return this._http.post(this._banksURL, bank);
  }

  updateBank(id: number, bank: Bank) {
    console.log(JSON.stringify(bank));
    return this._http.put(this._banksURL, id, bank);
  }

  deleteBank(id: number) {
    return this._http.delete(`${this._banksURL}/${id}`);
  }
}
