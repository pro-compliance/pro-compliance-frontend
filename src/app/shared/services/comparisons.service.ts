import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';

import { ConnectionService } from './connection.service';
import { Match, Comparison } from '../models/sanctions.model';
import { ParticipantsService } from './participants.service';
import { Participant } from '../models/participants.model';

@Injectable()
export class ComparisonsService {
  private _matchesURL: string;
  private _comparisonURL: string;
  private _participantURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService,
    private _partService: ParticipantsService
  ) {
    this._comparisonURL = _conn.APIUrl + 'comparisons';
    this._matchesURL = _conn.APIUrl + 'matches';
    this._participantURL = _conn.APIUrl + 'participants';
  }

  getComparisons() {
    return this._http.get(this._comparisonURL);
  }

  getMatchesbyComparison(id: number) {
    return this._http.get(`${this._comparisonURL}/${id}/matches`);
  }

  getMatchesByParticipant(id: number) {
    return this._http.get(`${this._participantURL}/${id}/matches`);
  }

  addComparison(fileName: string) {
    const _comparison: Comparison = {};
    _comparison.File = fileName;
    return this._http.post(this._comparisonURL, _comparison);
  }

  updateMatch(id: number, match: Match) {
    return this._http.put(this._matchesURL, id, match);
  }

  addMatch(match: Match) {
    return this._http.post(this._matchesURL, match);
  }

  runComparison(
    comparisonID: number,
    sheet: any[],
    columns: any[]
  ): Promise<any[]> {
    const matchs: any[] = [];
    return new Promise(resolve => {
      this._partService.getParticipants().subscribe(response => {
        const participants = <Participant[]>response.data;
        participants.forEach(participant => {
          const partTerm = participant.fullName.toLocaleLowerCase().split(' ');
          sheet.forEach(row => {
            let count = 0;
            const match = row;
            columns.forEach(field => {
              if (row[field.name]) {
                const terms = row[field.name].toLocaleLowerCase().split(' ');
                terms.forEach(term => {
                  if (partTerm.indexOf(term) >= 0 && term.length > 4) {
                    count = count + 1;
                  }
                });
              }
            });
            if (count > 1) {
              const val: any = {};
              val.participant = participant;
              val.score = count;
              val.match = match;
              matchs.push(val);
            }
          });
        });
        resolve(matchs);
      });
    });
  }
}
