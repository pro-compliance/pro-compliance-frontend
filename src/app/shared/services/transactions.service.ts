import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { TransactionSource, Transaction } from '../models/profiles.model';
import { AlertsService } from './alerts.service';
import { Alert } from '../models/alerts.model';
import { ToastrService } from 'ngx-toastr';

@Injectable()
export class TransactionsService {
  private _transactionsURL: string;
  private _sourcesURL: string;
  private _profilesURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _alertServ: AlertsService,
    private _conn: ConnectionService,
    private toast: ToastrService
  ) {
    this._transactionsURL = _conn.APIUrl + 'transactions';
    this._sourcesURL = _conn.APIUrl + 'transactionsources';
    this._profilesURL = _conn.APIUrl + 'participantprofiles';
  }

  getTransactions() {
    return this._http.get(this._transactionsURL);
  }

  getTransactionsByProfile(id: number) {
    return this._http.get(`${this._profilesURL}/${id}/transactions`);
  }

  createTransaction(tran: Transaction) {
    return this._http.post(this._transactionsURL, tran);
  }

  updateTransaction(id: number, tran: Transaction) {
    return this._http.put(this._transactionsURL, id, tran);
  }

  getSources() {
    return this._http.get(this._sourcesURL);
  }

  createSource(source: TransactionSource) {
    return this._http.post(this._sourcesURL, source);
  }

  editSource(id: number, source: TransactionSource) {
    return this._http.put(this._sourcesURL, id, source);
  }

  deleteSource(id: number) {
    return this._http.delete(`${this._sourcesURL}/${id}`);
  }

  generateAlert(participantID: number, reason: string, message?: string) {
    this._alertServ.getReason(reason).subscribe(response => {
      const data = response.data;
      const alert: Alert = {};
      alert.participantId = participantID;
      alert.alertReasonId = data.id;
      alert.alertSourceId = data.alertSourceId;
      alert.description = message;
      alert.createDate = new Date();
      this._alertServ.createAlert(alert).subscribe(datad => {
        this.toast.warning(alert.description, 'Alerta generada');
      });
    });
  }
}
