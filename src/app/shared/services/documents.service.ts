import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';
import { ConnectionService } from './connection.service';
import { DocumentType, ParticipantDocument } from '../models/documents.models';

@Injectable()
export class DocumentsService {
  private _documentURL: string;
  private _docTypeURL: string;
  private _partDocURL: string;
  private _partURL: string;

  constructor(
    private _http: CustomHttpClient,
    private _conn: ConnectionService
  ) {
    this._docTypeURL = _conn.APIUrl + 'documenttypes';
    this._documentURL = _conn.APIUrl + 'documents';
    this._partDocURL = _conn.APIUrl + 'participantdocuments';
    this._partURL = _conn.APIUrl + 'participants';
  }

  getTypes() {
    return this._http.get(this._docTypeURL);
  }

  getExpired() {
    return this._http.get(`${this._partDocURL}/expired`);
  }

  getTypesByParticipant(id: number) {
    return this._http.get(`${this._docTypeURL}/type/${id}`);
  }

  addType(type: DocumentType) {
    return this._http.post(this._docTypeURL, type);
  }

  updateType(id: number, type: DocumentType) {
    return this._http.put(this._docTypeURL, id, type);
  }

  deleteType(id: number) {
    return this._http.delete(`${this._docTypeURL}/${id}`);
  }

  getDocByParticipant(
    participantID: number
  ) {
    return this._http.get(
      `${this._partURL}/${participantID}/documents`
    );
  }

  saveDoc(doc: ParticipantDocument) {
    return this._http.post(this._partDocURL, doc);
  }

  updateDocument(id: number, doc: ParticipantDocument) {
    return this._http.put(this._partDocURL, id, doc);
  }

  deleteDoc(id: number) {
    return this._http.delete(`${this._partDocURL}/${id}`);
  }
}
