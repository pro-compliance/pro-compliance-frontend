import { CustomHttpClient } from './custom-http.service';
import { Injectable } from '@angular/core';

import { ConnectionService } from './connection.service';
import { ParamSubValue } from './../models/params.model';

@Injectable()
export class ParamSubValuesService {
  private _subValuesURL: string;

  constructor(private _http: CustomHttpClient, private _conn: ConnectionService) {
    this._subValuesURL = _conn.APIUrl + 'paramsubvalues';
  }

  addSubValue(val: ParamSubValue) {
    return this._http.post(this._subValuesURL, val);
  }

  editSubValue(id: number, val: ParamSubValue) {
    return this._http.put(this._subValuesURL, id, val);
  }

  deleteSubValue(id: number) {
    return this._http.delete(`${this._subValuesURL}/${id}`);
  }
}
