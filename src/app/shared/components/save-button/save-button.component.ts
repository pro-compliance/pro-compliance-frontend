import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-save-button',
  templateUrl: './save-button.component.html',
  styleUrls: ['./save-button.component.sass']
})
export class SaveButtonComponent implements OnInit {
  @Input() class = 'primary';
  @Input() size?: string;
  @Input() outline = false;


  constructor() { }

  ngOnInit() {
  }

}
