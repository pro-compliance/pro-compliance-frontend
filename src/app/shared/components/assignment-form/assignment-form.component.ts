import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Assignment, Progress } from '../../models/assignment.model';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable } from 'rxjs';
import { AssignmentsService } from '../../services/assignments.service';
import { ToastrService } from 'ngx-toastr';

const NOW = new Date();
@Component({
  selector: 'app-assignment-form',
  templateUrl: './assignment-form.component.html',
  styleUrls: ['./assignment-form.component.sass']
})
export class AssignmentFormComponent implements OnInit {
  @Input() currentAssignment?: Assignment;
  @Output() addAssignment = new EventEmitter();
  @Output() updateAssignment = new EventEmitter();

  _assignment: Assignment;
  _progresses: Observable<Progress[]>;
  _maxDate: any;
  _minDate: any;

  constructor(
    private _assignServ: AssignmentsService,
    public activeModal: NgbActiveModal,
    private toast: ToastrService
  ) {
    this._maxDate = { year: NOW.getFullYear() + 1, month: 12 };
    this._minDate = { year: NOW.getFullYear(), month: 1 };
  }

  ngOnInit() {
    this._progresses = this._assignServ.getProgress().map(response => {
      return response.data;
    });
  }

  save() {
    if (!this._assignment.id) {
      this._assignServ
        .createAssignment(this._assignment)
        .subscribe(response => {
          this.toast.success('Tarea creada');
          this._assignment = response.data;
          this.addAssignment.emit(this._assignment);
          this._assignment = {};
        });
    } else {
      this._assignServ
        .updateAssigment(this._assignment.id, this._assignment)
        .subscribe(response => {
          this.updateAssignment.emit(this._assignment);
          this._assignment = {};
        });
    }
  }
}
